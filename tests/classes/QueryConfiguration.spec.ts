import test from 'ava';
import { expect } from 'chai';

import queryTypes from '../../src/static/queryTypes';
import { QueryConfiguration } from '../../src/classes/QueryConfiguration';

test('should create a new instance of QueryConfiguration', (t) => {
  const p = new QueryConfiguration('simple', 'https://test.com');
  expect(p).to.be.an.instanceof(QueryConfiguration);
});

test('should should not create a new instance of QueryConfiguration if there is no type', (t) => {
  // @ts-ignore
  expect(() => new QueryConfiguration()).to.throw(Error, 'type must be defined');
});

test('should create a new Protocol with all availables models', (t) => {
  for (const queryType in queryTypes) {
    const p = new QueryConfiguration(queryType, 'http://test.com');
    expect(p.type).to.equal(queryType);
  };
});

test('should uppercase the type', (t) => {
  for (const queryType in queryTypes) {
    const p = new QueryConfiguration(queryType.toLowerCase(), 'https://test.com');
    expect(p.type).to.equal(queryType.toUpperCase());
  };
});

test('should throw an error if type doesn\'t exist', (t) => {
  expect(() => new QueryConfiguration('test', 'https://test.com')).to.throw(TypeError, 'Invalid QueryConfiguration type: test');
});

test('should throw an error if pollingStates.success is not defined correctly', (t) => {
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success : 'test' })).to.throw(Error, 'pollingStates success should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success : [] })).to.throw(Error, 'pollingStates success should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success : ['test'] })).to.throw(Error, 'pollingStates success should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success : ['test', []] })).to.throw(Error, 'pollingStates success should be defined correctly')
});

test('should throw an error if pollingStates.error is not defined correctly', (t) => {
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success: ['STATE', ['SUCCESS']], error : 'test' })).to.throw(Error, 'pollingStates error should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success: ['STATE', ['SUCCESS']], error : [] })).to.throw(Error, 'pollingStates error should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success: ['STATE', ['SUCCESS']], error : ['test'] })).to.throw(Error, 'pollingStates error should be defined correctly')
  //@ts-ignore
  expect(() => new QueryConfiguration('simple', 'https://test.com', { success: ['STATE', ['SUCCESS']], error : ['test', []] })).to.throw(Error, 'pollingStates error should be defined correctly')
});

test('should have default pollingStates', (t) => {
  const pollingStates = {
    success: ['STATE', ['SUCCESS']],
    error: ['STATE', ['ERROR']],
    running: ['STATE', ['RUNNING']],
    initialized: ['STATE', ['INITIALIZED']],
  };
  const p = new QueryConfiguration('simple', 'https://test.com');
  expect(p.pollingStates).to.exist;
  expect(p.pollingStates).to.eql(pollingStates)
});