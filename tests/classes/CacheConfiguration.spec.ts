import test from 'ava';
import { expect } from 'chai';

import cacheTypes from '../../src/static/cacheTypes';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import DataStore from '../../src/classes/DataStore';

const store = new DataStore({});

test('should create a new instance of CacheConfiguration', (t) => {
  const p = new CacheConfiguration('cache', store);
  expect(p).to.be.an.instanceof(CacheConfiguration);
});

test('should create a new CacheConfiguration with all availables models', (t) => {
  for (const type in cacheTypes) {
    const p = new CacheConfiguration(type, store);
    expect(p.type).to.equal(type);
  };
});

test('should should not create a new instance of CacheConfiguration if there is no type', (t) => {
  // @ts-ignore
  expect(() => new CacheConfiguration()).to.throw(Error, 'type must be defined');
});

test('should uppercase the type', (t) => {
  for (const type in cacheTypes) {
    const p = new CacheConfiguration(type.toLowerCase(), store);
    expect(p.type).to.equal(type.toUpperCase());
  };
});

test('should throw an error if type doesn\'t exist', (t) => {
  expect(() => new CacheConfiguration('test', store)).to.throw(TypeError, 'Invalid CacheConfiguration type: test');
});

test('should add the uniqueKeys to CacheConfiguration', (t) => {
  const uniqueKeys = ['id', 'test'];
  const p = new CacheConfiguration('cache', store, uniqueKeys);
  expect(p.uniqueKeys).to.equal(uniqueKeys);
});

test('should add storeModuleName to CacheConfiguration', (t) => {
  const uniqueKeys = ['id', 'test'];
  const storeModuleName = 'lines';
  const p = new CacheConfiguration('cache', store, uniqueKeys, storeModuleName);
  expect(p.storeModuleName).to.equal(storeModuleName);
});

test('should add optimisticResponse to CacheConfiguration', (t) => {
  const uniqueKeys = ['id', 'test'];
  const storeModuleName = 'lines';
  const optimisticResponse = {
    name: 'test',
  };
  const p = new CacheConfiguration('cache', store, uniqueKeys, storeModuleName, optimisticResponse);
  expect(p.optimisticResponse).to.equal(optimisticResponse);
});