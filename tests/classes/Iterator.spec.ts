import test from 'ava';
import { expect } from 'chai';

import Iterator from '../../src/classes/Iterator';

test('should create a new Iterator', (t) => {
  const iterator = new Iterator(['test']);
  expect(iterator).to.be.an.instanceOf(Iterator);
  expect(iterator.next).to.exist;
  expect(iterator.next).to.be.an.instanceOf(Function);
});

test('should iterate', (t) => {
  const iterator = new Iterator(['test', 'test2']);
  const testValues = [];

  for (const key of iterator) {
    testValues.push(key);
  }

  expect(testValues).to.eql(['test', 'test2']);
});

test('should get the next value', (t) => {
  const iterator = new Iterator(['test', 'test2']);

  expect(iterator.next()).to.eql({
    done: false,
    value: 'test',
  });
  expect(iterator.next()).to.eql({
    done: false,
    value: 'test2',
  });
  expect(iterator.next()).to.eql({
    done: true,
    value: null,
  });
  expect(iterator.next()).to.eql({
    done: true,
    value: null,
  });
});