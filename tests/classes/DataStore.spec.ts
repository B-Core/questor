import test from 'ava';
import { expect } from 'chai';

import { module, actionModule } from '../../src/types/module';
import DataStore from '../../src/classes/DataStore';

let store: DataStore;

test.beforeEach((t) => {
  const state: module = {
    module1: {},
    module2: [],
    module3: '',
    module4: 0,
  };
  const mutations: actionModule = {
    'set:module1' : (state: module, data: any) => {
      state.module1[data.id] = data;
      return state.module1[data.id];
    },
    'set:module2' : (state: module, data: any) => {
      state.module2.push(data);
      return state.module2;
    },
    'set:module3' : (state: module, data: any) => {
      state.module3 = data;
      return state.module3;
    },
    'set:module4' : (state: module, data: any) => {
      state.module4 = data;
      return state.module4;
    }
  };
  const actions: actionModule = {
    'async:set:module3' : (state: module, commit: Function, data: any): Promise<any> => {
      return new Promise((resolve, reject) => {
        setTimeout(() => {
          commit('set:module3', data);
          resolve();
        }, 1000);
      });
    },
  };
  const getters: actionModule = {
    'module1:keys': (state: module, get: Function): any => {
      return Object.keys(state.module1);
    }
  };
  store = new DataStore({state, mutations, actions, getters});
});

test('should correctly instanciate a DataStore', (t) => {
  expect(store).to.exist;
  expect(store).to.be.an.instanceof(DataStore);
});

test('should use default store state', (t) => {
  const st = new DataStore({});
  expect(st.state).to.eql({});
  expect(st.mutations).to.eql({});
  expect(st.actions).to.eql({});
  expect(st.getters).to.eql({});
});

test('should correctly get the state', (t) => {
  expect(store.state).to.eql({
    module1: {},
    module2: [],
    module3: '',
    module4: 0,
  });
});

test('should correctly set the state', (t) => {
  store.state = {
    testModule: {},
  };
  expect(store.state).to.eql({
    testModule: {},
  });
});

test('should correctly commit the data', (t) => {
  const objectData = { id: 'test' };
  store.commit('set:module1', objectData);
  expect(store.state.module1['test']).to.eql(objectData);

  store.commit('set:module2', objectData);
  expect(store.state.module2).to.contain(objectData);

  store.commit('set:module3', 'test');
  expect(store.state.module3).to.equal('test');

  store.commit('set:module4', 1);
  expect(store.state.module4).to.equal(1);
});

test('should throw an error if the mutation does\'nt exist', (t) => {
  expect(() => store.commit('test', 'fail')).to.throw(ReferenceError, 'no mutation test');
});

test('should pass correct arguments to mutation', (t) => {
  store.mutations.testMutation = (state: module, data: any): any => {
    expect(state).to.eql(store.state);
    expect(data).to.equal('test');
  };
  store.commit('testMutation', 'test');
});

test('should correctly do async operation with actions', (t) => {
  const testValue = 'test async';
  store.dispatch('async:set:module3', testValue).then(() => {
    expect(store.state.module3).to.equal(testValue);
  });
});

test('should throw an error if action does\'nt exist', (t) => {
  expect(() => store.dispatch('test', 'test')).to.throw(ReferenceError, 'no action test');
});

test('should pass correct arguments to actions', (t) => {
  store.actions.testAction = (state: module, commit: Function, data: any): Promise<any> => {
    expect(state).to.eql(store.state);
    expect(commit).to.be.an.instanceof(Function);
    expect(data).to.equal('test');
    return new Promise((resolve, reject) => {});
  }
  store.dispatch('testAction', 'test');
});

test('should correctly get the data', (t) => {
  const objectData = { id: 'test' };
  store.commit('set:module1', objectData);
  expect(store.get('module1:keys')).to.eql(['test']);
});

test('should throw an error if the getter does\'nt exist', (t) => {
  expect(() => store.get('test')).to.throw(ReferenceError, 'no getter test');
});

test('should pass correct arguments to getters', (t) => {
  store.getters.testGetter = (state: module, get: Function, test: string): any => {
    expect(state).to.eql(store.state);
    expect(get).to.be.an.instanceof(Function);
    expect(test).to.equal('test');
  };
  store.get('testGetter', 'test');
});