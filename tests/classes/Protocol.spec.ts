import test from 'ava';
import { expect } from 'chai';

import protocols from '../../src/static/protocols';
import Protocol from '../../src/classes/Protocol';

test('should create a new instance of Protocol', (t) => {
  const p = new Protocol('graphql');
  expect(p).to.be.an.instanceof(Protocol);
});

test('should should not create a new instance of Protocol if there is no type', (t) => {
  // @ts-ignore
  expect(() => new Protocol()).to.throw(Error, 'type must be defined');
});

test('should create a new Protocol with all availables models', (t) => {
  for (const protocol in protocols) {
    const p = new Protocol(protocol);
    expect(p.type).to.equal(protocol);
  };
});

test('should uppercase the type', (t) => {
  for (const protocol in protocols) {
    const p = new Protocol(protocol.toLowerCase());
    expect(p.type).to.equal(protocol.toUpperCase());
  };
});

test('should throw an error if type doesn\'t exist', (t) => {
  expect(() => new Protocol('test')).to.throw(TypeError, 'Invalid Protocol type: test');
});