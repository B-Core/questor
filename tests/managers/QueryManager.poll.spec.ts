import test from 'ava';
import { expect } from 'chai';

// @ts-ignore
import { findIndex } from 'lodash/array';
// @ts-ignore
import { cloneDeep } from 'lodash/lang';

const nock = require('nock');

import Questor from '../../src/Questor';
import QueryManager from '../../src/managers/QueryManager';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import Body from '../../src/models/Body';
import Request from '../../src/interfaces/Request';
import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import RESTRequest from '../../src/models/RESTRequest';
import GraphqlQueryConfiguration from '../../src/models/GraphqlQueryConfiguration';
import GraphqlRequest from '../../src/models/GraphqlRequest';
import DataStore from '../../src/classes/DataStore';

const processResponseRunning = {
  objectId: 'test',
  STATE: 'RUNNING',
}

const processResponseInitialized = {
  objectId: 'test',
  STATE: 'INITIALIZED',
}

const processResponseFailed = {
  objectId: 'test',
  STATE: 'ERROR',
}

const processResponseSuccess = {
  objectId: 'test',
  STATE: 'SUCCESS',
}

const internalServerError = {
  message: 'Internal Server Error',
};


let store: DataStore;

const nockEndpoint = nock('https://success.com')
                    .post('/')
                    .reply(201, processResponseInitialized)
                    .post('/')
                    .times(10)
                    .reply(200, processResponseRunning)
                    .post('/')
                    .reply(200, processResponseSuccess);

const nockEndpointFail = nock('https://fail.com')
                    .post('/')
                    .reply(201, processResponseInitialized)
                    .post('/')
                    .times(3)
                    .reply(200, processResponseRunning)
                    .post('/')
                    .reply(200, processResponseFailed);

const nockEndpointPollLimit = nock('https://limit.com')
                    .post('/')
                    .reply(201, processResponseInitialized)
                    .post('/')
                    .times(300)
                    .reply(200, processResponseRunning)

const nockEndpointPollWithError = nock('https://error.com')
                    .post('/')
                    .reply(201, processResponseInitialized)
                    .post('/')
                    .times(3)
                    .reply(200, processResponseRunning)
                    .post('/')
                    .reply(500, internalServerError);

const nockEndpointnoInit = nock('https://noinit.com')
                    .post('/')
                    .times(10)
                    .reply(200, processResponseRunning)
                    .post('/')
                    .reply(200, processResponseSuccess);

test.beforeEach((t) => {
  store = new DataStore({
    state: {
      lines: [],
    },
    mutations: {
      'set:lines': (state: any, searchKeys: any, data: any): any => {
        if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) {
           if (data) state.lines.splice(index, 1, data);
          } else {
            state.lines.push(data);
          }
          return data;
        }
      },
      'delete:lines': (state: any, searchKeys: any, data: any): void => {
        if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) state.lines.splice(index, 1)
        }
      },
    }
  });
});

const line1 = {
  name: 'test',
  id: 'test',
  tactics: [
    {
      id: 'tactic-test',
      name: 'tactic-test',
    }
  ],
}

test('should poll and wait for success', async (t) => {
  const st: DataStore = cloneDeep(store);
  const questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', line1),
    queryConfig: new RESTQueryConfiguration('POST', 'polling', 'https://success.com'),
  });

  await new Promise((resolve, reject) => {
    const requestResult = questor.send(questor.createRequest())
    .then((response) => {
      expect(st.state.lines).to.have.lengthOf(1);
      expect(st.state.lines[0]).to.eql(line1);
      resolve();
    });
    expect(st.state.lines).to.have.lengthOf(1);
    expect(st.state.lines[0]).to.eql(line1);
  });
});

test('should poll and receive an error, and pop body', async (t) => {
  const st: DataStore = cloneDeep(store);
  const questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new RESTQueryConfiguration('POST', 'polling', 'https://error.com', new Body(line1)),
  });

  await new Promise((resolve, reject) => {
    const requestResult = questor.send(questor.createRequest())
    .catch((response) => {
      expect(st.state.lines).to.have.lengthOf(0);
      resolve();
    });
    expect(st.state.lines).to.have.lengthOf(1);
    expect(st.state.lines[0]).to.eql(line1);
  })
});

test('should poll and wait for failure, and pop optimisticResponse', async (t) => {
  const st: DataStore = cloneDeep(store);
  const questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', line1),
    queryConfig: new RESTQueryConfiguration('POST', 'polling', 'https://fail.com'),
  });

  await new Promise((resolve, reject) => {
    const requestResult = questor.send(questor.createRequest())
    .catch((response) => {
      expect(st.state.lines).to.have.lengthOf(0);
      resolve();
    });
    expect(st.state.lines).to.have.lengthOf(1);
    expect(st.state.lines[0]).to.eql(line1);
  })
});

test('should poll and fail because of pollLimit', async (t) => {
  const st: DataStore = cloneDeep(store);
  const questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new RESTQueryConfiguration('POST', 'polling', 'https://limit.com', new Body(line1)),
  });
  questor.queryManager.pollLimit = 10;

  await new Promise(async (resolve, reject) => {
    try {
      await questor.send(questor.createRequest());
    } catch(e) {
      expect(st.state.lines).to.have.lengthOf(0);
      resolve();
    }
  });
});