import test from 'ava';
import { expect } from 'chai';

// @ts-ignore
import { findIndex } from 'lodash/array';
// @ts-ignore
import { cloneDeep } from 'lodash/lang';

const nock = require('nock');

import Questor from '../../src/Questor';
import QueryManager from '../../src/managers/QueryManager';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import Body from '../../src/models/Body';
import Request from '../../src/interfaces/Request';
import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import RESTRequest from '../../src/models/RESTRequest';
import GraphqlQueryConfiguration from '../../src/models/GraphqlQueryConfiguration';
import GraphqlRequest from '../../src/models/GraphqlRequest';
import DataStore from '../../src/classes/DataStore';

const N_OF_REQUEST = 10;

const jsonResponse = {
  message: 'fetched !',
};

const jsonResponse2 = {
  id: 'test',
  desc: 'test desc',
  childs: [
    {
      id: 'test-child',
      name: 'test-child',
      desc: 'test-desc-child',
    }
  ],
};

const jsonResponse3 = {
  id: 'test',
  desc: 'test desc',
  childs: [
    {
      id: 'test-child',
      name: 'test-child',
      desc: 'test-desc-child',
    },
    {
      id: 'test-child-2',
      name: 'test-child=2',
      desc: 'test-desc-child-2',
    }
  ],
};

const internalServerError = {
  message: 'Internal Server Error',
};

const nockEndpoint = nock('https://test.com')
                    .get('/')
                    .times(N_OF_REQUEST)
                    .reply(200, jsonResponse);

const nockEndpoint2 = nock('https://test2.com')
                    .get('/')
                    .times(N_OF_REQUEST)
                    .reply(500, internalServerError);


const nockEndpoint3 = nock('https://test3.com')
                    .get('/')
                    .reply(200, jsonResponse2)
                    .get('/')
                    .delay(300)
                    .reply(200, jsonResponse3)
                    .get('/')
                    .delay(300)
                    .reply(200, jsonResponse2)
                    .get('/')
                    .delay(300)
                    .reply(200, jsonResponse3)
                    .post('/')
                    .delay(500)
                    .reply(500, jsonResponse3);

const nockEndpoint4 = nock('https://test4.com')
                    .get('/')
                    .reply(500, internalServerError);    
                    
const nockEndpoint5 = nock('https://test5.com')
                    .get('/')
                    .times(N_OF_REQUEST)
                    .reply(200, jsonResponse); 
                   
const nockEndpoint6 = nock('https://test6.com')
                    .post('/')
                    .times(N_OF_REQUEST)
                    .reply(500, internalServerError);               

const nockEndpoint7 = nock('https://test7.com')
                    .post('/')
                    .times(1)
                    .reply(500, internalServerError);                      

 const nockEndpoint8 = nock('https://test8.com')
                    .post('/')
                    .times(100)
                    .reply(500, internalServerError);                      

 const nockEndpoint9 = nock('https://test8.com')
                    .get('/')
                    .times(100)
                    .reply(500, internalServerError);                      


let questor: Questor;
let store: DataStore;

test.beforeEach((t) => {
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test.com'),
  });
  store = new DataStore({
    state: {
      orders: [],
      lines: [],
    },
    mutations: {
      'set:lines': (state: any, searchKeys: any, data: any): any => {
        if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) {
            state.lines.splice(index, 1, data);
          } else {
            state.lines.push(data);
          }
          return data;
        }
      },
      'delete:lines': (state: any, searchKeys: any, data: any): void => {
        if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) state.lines.splice(index, 1)
        }
      },
      'set:orders': (state: any, searchKeys: any, data: any): any => {
        if (data && data[searchKeys[0]]) {
          const index = findIndex(state.orders, { id: data[searchKeys[0]] });
          if (index >= 0) {
            state.orders.splice(index, 1, data);
          } else {
            state.orders.push(data);
          }
          return data;
        }
      },
    }
  });
});

test('should create request and add it to pending', (t) => {
  const queryManager = new QueryManager();
  expect(Object.keys(queryManager._store.state.pending)).to.have.lengthOf(0);
});

test('should add request to resolved if request resolved', async (t) => {

  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test.com'),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  const requests = await queryManager.executeAll();
  expect(requests).to.have.lengthOf(N_OF_REQUEST);
  expect(queryManager.pendingRequests).to.have.lengthOf(0);

  await setTimeout(() => {
    requests.forEach(async (request: Request) => {
      expect(await request.response.json()).to.eql(jsonResponse);
    });
    expect(queryManager.resolvedRequests).to.have.lengthOf(N_OF_REQUEST);
  }, 1000);
});

test('should add request to error if requests failed', async (t) => {

  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test2.com'),
  });

  const queryManager = new QueryManager();
  const refReq: Array<Request> = [];

  for (let i = 0; i < N_OF_REQUEST; i++) {
    refReq.push(questor.createRequest());
  }
  queryManager.add(...refReq);

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);

  let requests: Array<Request>;
  try {
    requests = await queryManager.executeAll();
  } catch(e) {
    await setTimeout(() => {
      expect(requests).to.not.exist;
      expect(queryManager.pendingRequests).to.have.lengthOf(0);
      expect(queryManager.failedRequests).to.have.lengthOf(N_OF_REQUEST);
      queryManager.failedRequests.forEach((id, index) => expect(id).to.equal(refReq[index].id));
    }, 1000);
  }
});

test('should fail on error', async (t) => {

  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', store),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test8.com'),
  });

  const queryManager = new QueryManager();
  const request = questor.createRequest();

  try {
   const r = await queryManager.execute(request);
  } catch(e) {
    expect(queryManager.pendingRequests).to.have.lengthOf(0);
    expect(queryManager.failedRequests).to.have.lengthOf(1);
  }
});

test('should add request and return cached results', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
  });

  const body = new Body({
    name: 'test',
    id: 'test',
  });

  const queryManager = new QueryManager();
  const request1 = questor.createRequest(
    new Protocol('rest'),
    new RESTQueryConfiguration('GET', 'simple', 'https://test3.com'),
  );
  const request2 = questor.createRequest(
    new Protocol('rest'),
    new RESTQueryConfiguration('GET', 'simple', 'https://test3.com'),
  );
  const request3 = questor.createRequest(
    new Protocol('rest'),
    new RESTQueryConfiguration('GET', 'simple', 'https://test3.com'),
    new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse2),
  );
  const request4 = questor.createRequest(
    new Protocol('rest'),
    new RESTQueryConfiguration('GET', 'simple', 'https://test3.com'),
    new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse2),
  );
  const request5 = questor.createRequest(
    new Protocol('rest'),
    new RESTQueryConfiguration('POST', 'simple', 'https://test3.com', body),
    new CacheConfiguration('cache', st, ['id'], 'lines'),
  );

  queryManager.add(request1, request2, request3, request4, request5);

  expect(queryManager.pendingRequests).to.have.lengthOf(5);
  expect(queryManager.resolvedRequests).to.have.lengthOf(0);
  
  const request1result = await queryManager.execute(request1);

  expect(queryManager.pendingRequests).to.have.lengthOf(4);
  expect(queryManager.resolvedRequests).to.have.lengthOf(1);

  await new Promise((resolve, reject) => {
    setTimeout(async () => {
      expect(st.state.lines).to.have.lengthOf(1);
      expect(st.state.lines[0].childs).to.have.lengthOf(1);
      const request2result = await queryManager.execute(request2);
  
      expect(queryManager.pendingRequests).to.have.lengthOf(3);
      expect(queryManager.resolvedRequests).to.have.lengthOf(2);
  
      expect(st.state.lines).to.have.lengthOf(1);
      expect(st.state.lines).to.eql([jsonResponse2]);
      await setTimeout(async () => {
        expect(st.state.lines[0].childs).to.have.lengthOf(2);
        expect(st.state.lines).to.eql([jsonResponse3]);

        const request3result = await queryManager.execute(request3);

        expect(queryManager.pendingRequests).to.have.lengthOf(2);
        expect(queryManager.resolvedRequests).to.have.lengthOf(3);

        expect(st.state.lines[0].childs).to.have.lengthOf(1);
        expect(st.state.lines).to.eql([jsonResponse2]);

        await setTimeout(async () => {
          expect(st.state.lines[0].childs).to.have.lengthOf(1);
          expect(st.state.lines).to.eql([jsonResponse2]);
          const request4result = await queryManager.execute(request4);
          
          expect(queryManager.pendingRequests).to.have.lengthOf(1);
          expect(queryManager.resolvedRequests).to.have.lengthOf(4);

          expect(st.state.lines[0].childs).to.have.lengthOf(1);
          expect(st.state.lines).to.eql([jsonResponse2]);
  
          await setTimeout(async () => {
            expect(st.state.lines[0].childs).to.have.lengthOf(2);
            expect(st.state.lines).to.eql([jsonResponse3]);

            let request5result;
            try {
              request5result = await queryManager.execute(request5);
            } catch(req) {
              expect(req.cacheConfig.backup).to.eql(jsonResponse3);
              expect(st.state.lines).to.have.lengthOf(1);
              expect(st.state.lines).to.eql([body]);
              await setTimeout(() => {
                expect(st.state.lines).to.have.lengthOf(1);
                expect(st.state.lines).to.eql([jsonResponse3]);
                
                resolve();
              }, 600);
            }
            
          }, 500);
        }, 500);
      }, 500);
    }, 300);
  })
});

test('should use successCallback path', async (t) => {
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', cloneDeep(store), ['id'], 'lines', undefined, (response: Response) => {
      if (response) expect(response).to.eql(jsonResponse);
    }),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test5.com'),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  const requests = await queryManager.executeAll();
  expect(requests).to.have.lengthOf(N_OF_REQUEST);
  expect(queryManager.pendingRequests).to.have.lengthOf(0);
});

test('should use errorCallback path', async (t) => {
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', store, ['id'], 'lines', undefined, undefined, (response: Response) => {
      if (response) expect(response).to.eql(internalServerError);
    }),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test6.com', new Body({
      id: 'test',
      name: 'test',
    })),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  let requests: Array<Request>;
  try {
    requests = await queryManager.executeAll();
  } catch(e) {
    expect(requests).to.not.exist;
    expect(queryManager.pendingRequests).to.have.lengthOf(0);
    expect(queryManager.failedRequests).to.have.lengthOf(N_OF_REQUEST);
  }
});

test('should execute cache update event if request fails for no network reason', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test8.com', new Body({
      id: 'test',
      name: 'test',
    })),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(1);
  }
});

test('should execute cache update event if request fails', async (t) => {
  const st = cloneDeep(store);
  const json = {
    id: 'test',
    name: 'test',
  };
  const q = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', undefined, undefined, (response: Response) => {
      if (response) expect(response).to.eql(internalServerError);
    }),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test7.com', new Body(json)),
  });
  const queryManager = new QueryManager();
  const request = q.createRequest();

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(1);
  }
});

test('should execute cache update with optimistic response', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse3),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test8.com'),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(1);
  }
});

test('should execute pop state if state is object and request failure', async (t) => {
  const st = new DataStore({
    state: {
      lines: {},
    },
    mutations: {
      'set:lines': (state: any, data: any): any => {
        if (data && data.id) {
          state.lines[data.id] = data;
          return data;
        }
      },
      'delete:lines': (state: any, data: any): void => {
        if (data && data.id) {
          delete state.lines[data.id];
        }
      },
    }
  });
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test8.com', new Body({
      id: 'test',
      name: 'test',
    })),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {
    await setTimeout(() => {
      expect(st.state.lines).to.not.haveOwnProperty('test');
      expect(Object.keys(st.state.lines)).to.have.lengthOf(0);
    }, 100);
  }
});

test('should execute pop state if state is object and request failure and state already exist', async (t) => {
  const st = new DataStore({
    state: {
      lines: {
        'test': {
          id: 'test',
          name: 'test',
        }
      },
    },
    mutations: {
      'set:lines': (state: any, searchKeys: any, data: any): any => {
        if (data && data[searchKeys[0]]) {
          state.lines[data[searchKeys[0]]] = data;
          return data;
        }
      },
      'delete:lines': (state: any, searchKeys: any, data: any): void => {
        if (data && data[searchKeys[0]]) {
          delete state.lines[data[searchKeys[0]]];
        }
      },
    }
  });
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new RESTQueryConfiguration('POST', 'simple', 'https://test8.com', new Body({
      id: 'test',
      name: 'test',
      desc: 'test desc',
    })),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {
    expect(st.state.lines['test']).to.haveOwnProperty('desc');
    await setTimeout(() => {
      expect(st.state.lines).to.have.haveOwnProperty('test');
      expect(Object.keys(st.state.lines)).to.have.lengthOf(1);
      expect(st.state.lines['test']).to.not.haveOwnProperty('desc');
    }, 100);
  }
});