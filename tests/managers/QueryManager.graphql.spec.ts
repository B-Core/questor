import test from 'ava';
import { expect } from 'chai';

// @ts-ignore
import { findIndex } from 'lodash/array';
// @ts-ignore
import { cloneDeep } from 'lodash/lang';

const nock = require('nock');

import Questor from '../../src/Questor';
import QueryManager from '../../src/managers/QueryManager';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import Body from '../../src/models/Body';
import Request from '../../src/interfaces/Request';
import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import RESTRequest from '../../src/models/RESTRequest';
import GraphqlQueryConfiguration from '../../src/models/GraphqlQueryConfiguration';
import GraphqlRequest from '../../src/models/GraphqlRequest';
import DataStore from '../../src/classes/DataStore';

const N_OF_REQUEST = 3;

const jsonResponse1 = [
  {
    id: 'test',
    name: 'test',
    childs: [
      {
        id: 'test-child',
        name: 'test-child',
      }
    ],
  },
  {
    id: 'test2',
    name: 'test2',
    childs: [
      {
        id: 'test2-child',
        name: 'test2-child',
      }
    ],
  },
  {
    id: 'test3',
    name: 'test3',
    childs: [
      {
        id: 'test3-child',
        name: 'test3-child',
      },
      {
        id: 'test3-child',
        name: 'test3-child',
      }
    ],
  },
];

const jsonResponse2 = [
  {
    id: 'test',
    name: 'test',
    childs: [],
  },
  {
    id: 'test2',
    name: 'test2',
    childs: [
      {
        id: 'test2-child',
        name: 'test2-child',
      },
      {
        id: 'test2-child2',
        name: 'test2-child2',
      }
    ],
  },
];

const jsonResponse3 = {
  id: 'test',
  name: 'test',
};

const internalServerError = {
  message: 'Internal Server Error',
};

const lineQuery = `query lines($first: Int!) {
  lines(first: $first) {
    id
    name
    childs {
      id
      name
    }
  }
}`;

const lineMutation = `mutation line($input: LineInput!) {
  line(input: $input) {
    id
    name
    childs {
      id
      name
    }
  }
}`;

const lineVariables = {
  first: 3,
};

const lineInput = {
  id: 'test',
  name: 'test',
  // @ts-ignore
  childs: [
    {
      id: 'test-child',
      name: 'test-child',
    }
  ],
};

const nockEndpoint = nock('https://test.com')
                    .post('/')
                    .times(N_OF_REQUEST)
                    .reply(200, jsonResponse1);

const nockEndpoint2 = nock('https://test2.com')
                    .post('/')
                    .times(N_OF_REQUEST)
                    .reply(500, internalServerError);

const nockEndpoint3 = nock('https://test3.com')
                    .post('/')
                    .reply(200, jsonResponse1)
                    .post('/')
                    .delay(300)
                    .reply(200, jsonResponse2)
                    .post('/')
                    .delay(300)
                    .reply(200, jsonResponse1)
                    .post('/')
                    .delay(300)
                    .reply(200, jsonResponse2)
                    .post('/')
                    .delay(500)
                    .reply(500, internalServerError);

const nockEndpoint4 = nock('https://test4.com')
                    .post('/')
                    .times(N_OF_REQUEST)
                    .reply(200, jsonResponse1);
const nockEndpoint5 = nock('https://test5.com')
                    .post('/')
                    .times(N_OF_REQUEST)
                    .reply(500, internalServerError);

const nockEndpoint6 = nock('https://test6.com')
                    .post('/')
                    .times(100)
                    .reply(500, internalServerError);

const nockEndpoint7 = nock('https://test7.com')
                    .post('/')
                    .reply(200, jsonResponse3);
let questor: Questor;
let store: DataStore;

test.beforeEach((t) => {
  const storeArgs = {
    state: {
      lines: [],
    },
    mutations: {
      'set:lines': (state: any, searchKeys: any, data: any): any => {
        if (data instanceof Array) {
          state.lines = data;
        } else if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) {
            state.lines.splice(index, 1, data);
          } else {
            state.lines.push(data);
          }
          return data;
        }
      },
      'delete:lines': (state: any, searchKeys: any, data: any): void => {
        if (data instanceof Array) {
          data.forEach((d) => {
            const index = findIndex(state.lines, { id: d[searchKeys[0]] });
            if (index >= 0) state.lines.splice(index, 1, d)
          });
        } else if (data && data[searchKeys[0]]) {
          const index = findIndex(state.lines, { id: data[searchKeys[0]] });
          if (index >= 0) state.lines.splice(index, 1)
        }
      },
    }
  };
  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test.com'),
  });
  store = new DataStore(storeArgs);
});

test('should add request to resolved if request resolved', async (t) => {
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test.com', lineQuery, lineVariables),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  const requests = await queryManager.executeAll();
  expect(requests).to.have.lengthOf(N_OF_REQUEST);
  expect(queryManager.pendingRequests).to.have.lengthOf(0);

  await setTimeout(() => {
    requests.forEach(async (request: Request) => expect(await request.response.json()).to.eql(jsonResponse1));
    expect(queryManager.resolvedRequests).to.have.lengthOf(N_OF_REQUEST);
  }, 1000);
});

test('should add request to error if requests failed', async (t) => {
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test2.com', lineQuery, lineVariables),
  });

  const queryManager = new QueryManager();
  const refReq: Array<Request> = [];

  for (let i = 0; i < N_OF_REQUEST; i++) {
    refReq.push(questor.createRequest());
  }
  queryManager.add(...refReq);

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);

  let requests: Array<Request>;
  try {
    requests = await queryManager.executeAll();
  } catch(e) {
    await setTimeout(() => {
      expect(requests).to.not.exist;
      expect(queryManager.pendingRequests).to.have.lengthOf(0);
      expect(queryManager.failedRequests).to.have.lengthOf(N_OF_REQUEST);
      queryManager.failedRequests.forEach((id, index) => expect(id).to.equal(refReq[index].id));
    }, 1000);
  }
});

test('should add request and return cached results', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
  });

  const queryManager = new QueryManager();
  const request1 = questor.createRequest(
    new Protocol('graphql'),
    new GraphqlQueryConfiguration('query', 'simple', 'https://test3.com', lineQuery, lineVariables),
  );
  const request2 = questor.createRequest(
    new Protocol('graphql'),
    new GraphqlQueryConfiguration('query', 'simple', 'https://test3.com', lineQuery, lineVariables),
  );
  const request3 = questor.createRequest(
    new Protocol('graphql'),
    new GraphqlQueryConfiguration('query', 'simple', 'https://test3.com', lineQuery, lineVariables),
    new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse2),
  );
  const request4 = questor.createRequest(
    new Protocol('graphql'),
    new GraphqlQueryConfiguration('query', 'simple', 'https://test3.com', lineQuery, lineVariables),
    new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse2),
  );
  const request5 = questor.createRequest(
    new Protocol('graphql'),
    new GraphqlQueryConfiguration('mutation', 'simple', 'https://test3.com', lineMutation, lineInput),
    new CacheConfiguration('cache', st, ['id'], 'lines'),
  );

  queryManager.add(request1, request2, request3, request4, request5);

  expect(queryManager.pendingRequests).to.have.lengthOf(5);
  expect(queryManager.resolvedRequests).to.have.lengthOf(0);
  
  const request1result = await queryManager.execute(request1);

  expect(queryManager.pendingRequests).to.have.lengthOf(4);
  expect(queryManager.resolvedRequests).to.have.lengthOf(1);

  await new Promise((resolve, reject) => {
    setTimeout(async () => {
      expect(st.state.lines).to.have.lengthOf(3);
      expect(st.state.lines[0].childs).to.have.lengthOf(1);
      const request2result = await queryManager.execute(request2);
  
      expect(queryManager.pendingRequests).to.have.lengthOf(3);
      expect(queryManager.resolvedRequests).to.have.lengthOf(2);
  
      expect(st.state.lines).to.have.lengthOf(3);
      expect(st.state.lines).to.eql(jsonResponse1);
      await setTimeout(async () => {
        expect(st.state.lines[0].childs).to.have.lengthOf(0);
        expect(st.state.lines).to.eql(jsonResponse2);

        const request3result = await queryManager.execute(request3);

        expect(queryManager.pendingRequests).to.have.lengthOf(2);
        expect(queryManager.resolvedRequests).to.have.lengthOf(3);

        expect(st.state.lines[0].childs).to.have.lengthOf(0);
        expect(st.state.lines).to.eql(jsonResponse2);

        await setTimeout(async () => {
          expect(st.state.lines[0].childs).to.have.lengthOf(1);
          expect(st.state.lines).to.eql(jsonResponse1);
          const request4result = await queryManager.execute(request4);
          
          expect(queryManager.pendingRequests).to.have.lengthOf(1);
          expect(queryManager.resolvedRequests).to.have.lengthOf(4);

          expect(st.state.lines[0].childs).to.have.lengthOf(0);
          expect(st.state.lines).to.eql(jsonResponse2);
  
          await setTimeout(async () => {
            expect(st.state.lines[0].childs).to.have.lengthOf(0);
            expect(st.state.lines).to.eql(jsonResponse2);

            let request5result;
            try {
              request5result = await queryManager.execute(request5);
            } catch(e) {
              expect(request5.cacheConfig.backup).to.eql({ id: 'test', name: 'test', childs: [] });
              const mutResponse = cloneDeep(jsonResponse2);
              mutResponse[0].childs.push({ id: 'test-child', name: 'test-child' });
              expect(st.state.lines[0].childs).to.have.lengthOf(1);
              expect(st.state.lines).to.eql(mutResponse);
              await setTimeout(() => {
                expect(st.state.lines).to.have.lengthOf(2);
                expect(st.state.lines).to.eql(jsonResponse2);
                expect(st.state.lines[0].childs).to.have.lengthOf(0);
                resolve();
              }, 600);
            }
          }, 500);
        }, 500);
      }, 500);
    }, 300);
  })
});

test('should use successCallback path', async (t) => {
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', cloneDeep(store), ['id'], 'lines', undefined, (response: Response) => {
      if (response) expect(response).to.eql(jsonResponse1);
    }),
    queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test4.com', lineQuery, lineVariables),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  const requests = await queryManager.executeAll();
  expect(requests).to.have.lengthOf(N_OF_REQUEST);
  expect(queryManager.pendingRequests).to.have.lengthOf(0);
});

test('should use errorCallback path', async (t) => {
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', store, ['id'], 'lines', undefined, undefined, (response: Response) => {
      if (response) expect(response).to.eql(internalServerError);
    }),
    queryConfig: new GraphqlQueryConfiguration('mutation', 'simple', 'https://test5.com', lineMutation, lineInput),
  });

  const queryManager = new QueryManager();
  
  for (let i = 0; i < N_OF_REQUEST; i++) {
    queryManager.add(questor.createRequest());
  }

  expect(queryManager.pendingRequests).to.have.lengthOf(N_OF_REQUEST);
  let requests: Array<Request>;
  try {
    requests = await queryManager.executeAll();
  } catch(e) {
    expect(requests).to.not.exist;
    expect(queryManager.pendingRequests).to.have.lengthOf(0);
    expect(queryManager.failedRequests).to.have.lengthOf(N_OF_REQUEST);
  }
});

test('should execute cache update event if request fails for no network reason', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines'),
    queryConfig: new GraphqlQueryConfiguration('mutation', 'simple', 'https://test6.com', lineMutation, lineInput),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(1);
  }
});

test('should execute cache update event if request fails', async (t) => {
  const st = cloneDeep(store);
  const q = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', undefined, undefined, (response: Response) => {
      if (response) expect(response).to.eql(internalServerError);
    }),
    queryConfig: new GraphqlQueryConfiguration('mutation', 'simple', 'https://test6.com', lineMutation, lineInput),
  });
  const queryManager = new QueryManager();
  const request = q.createRequest();

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(1);
  }
});

test('should execute cache update with optimistic response', async (t) => {
  const st = cloneDeep(store);
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', jsonResponse2),
    queryConfig: new GraphqlQueryConfiguration('mutation', 'simple', 'https://test6.com', lineMutation, lineInput),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.have.lengthOf(2);
  }
});

test('should execute if state is object', async (t) => {
  const st = new DataStore({
    state: {
      lines: {},
    },
    mutations: {
      'set:lines': (state: any, searchKeys: any, data: any): any => {
        if (data && data[searchKeys[0]]) {
          state.lines[data[searchKeys[0]]] = data;
          return data;
        }
      },
      'delete:lines': (state: any, searchKeys: any, data: any): void => {
        if (data && data[searchKeys[0]]) {
          delete state.lines[data[searchKeys[0]]];
        }
      },
    }
  });
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', {
      id: 'test',
      name: 'test',
    }),
    queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test7.com', lineQuery, lineVariables),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {} finally {
    expect(st.state.lines).to.haveOwnProperty('test');
  }
});

test('should execute pop state if state is object and request failure', async (t) => {
  const st = new DataStore({
    state: {
      lines: {},
    },
    mutations: {
      'set:lines': (state: any, data: any): any => {
        if (data && data.id) {
          state.lines[data.id] = data;
          return data;
        }
      },
      'delete:lines': (state: any, data: any): void => {
        if (data && data.id) {
          delete state.lines[data.id];
        }
      },
    },
  });
  questor = new Questor({
    protocol: new Protocol('graphql'),
    cacheConfig: new CacheConfiguration('cache', st, ['id'], 'lines', {
      id: 'test',
      name: 'test',
    }),
    queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test6.com', lineQuery, lineVariables),
  });
  const queryManager = new QueryManager();
  const request = questor.createRequest();
  queryManager.add(request);

  try {
    const r = await queryManager.execute(request);
  } catch(e) {
    await setTimeout(() => {
      expect(st.state.lines).to.not.haveOwnProperty('test');
      expect(Object.keys(st.state.lines)).to.have.lengthOf(0);
    }, 100);
  }
});