module.exports = () => {
  console.log('executing test sendingRESTRequest.e2e.js');

  return new Promise((resolve, reject) => {
    const panQuery = `query QueryDiscoveryReport ($params: DiscoveryParams!) {
      discoveryReport (params: $params) {
        results {
          bidRequest
        }
      }
    }`;
  
    const panVariables = {
      params: {
        interval: {
          start: "2017-12-06T23:00:00.000Z",
          end: "2018-01-06T23:00:00.000Z",
        },
        granularity: 'NONE',
        groupBy: [],
      },
    };
      const questor = new Questor();
      const conf = Questor.createQueryConfiguration('graphql', {
        type: 'simple',
        method: 'query',
        url: 'http://marathon-lb.marathon.mesos.evo:31014/graphql',
        query: panQuery,
        variables: panVariables,
      });
      const cache = Questor.createCacheConfiguration({
        type: 'no-cache',
      });
      const request = questor.createRequest(
        {
          type: 'GRAPHQL'
        },
        conf,
        cache,
        {
          authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJuYW1lIjoiU3VwZXIgQWRtaW4iLCJlbWFpbCI6InN1cGVyYWRtaW5AczRtLmlvIiwiaXNzIjoiaHR0cDpcL1wvcHJvamVjdC1zc28uZGV2Iiwic3ViIjoiczRtLWZ1bGxzdGFja3w0IiwiYXVkIjpbeyJuYW1lIjoiU1NPIiwiaWRlbnRpZmllciI6InNzbyIsImhvc3QiOiJtYXJhdGhvbi1sYi5tYXJhdGhvbi5tZXNvcy5ldm86MzEwMTkifSx7Im5hbWUiOiJGdXNpbyIsImlkZW50aWZpZXIiOiJhZHNlcnZlciIsImhvc3QiOiJkZXYtYWR0cmFja2VydWkuc2FtNG0uY29tIn0seyJuYW1lIjoiRFNQIiwiaWRlbnRpZmllciI6ImRzcCIsImhvc3QiOiJtYXJhdGhvbi1sYi5tYXJhdGhvbi5tZXNvcy5ldm86MzEwMTMifV0sImV4cCI6MTUyNDY4NTgzNywibmJmIjoxNTI0NjQyNjM3LCJpYXQiOjE1MjQ2NDI2MzcsImp0aSI6IjJlZWZkM2EyODIwYjQwNGZhNGUxMTE3ZDMxMjZmNDk0OGU3Nzg1YjMiLCJwYXlsb2FkLXR5cGUiOiJSb2xlIiwiYXV0aF90aW1lIjoiMjAxOC0wNC0yNSAwOTo1MDozNyIsImF1dGhvcml6YXRpb24iOlt7ImNsaWVudCI6InNzbyIsInJvbGVzIjpbeyJpZCI6MiwibmFtZSI6IlNTT19VU0VSX01BTkFHRVIiLCJyZXNvdXJjZSI6eyJjbGllbnRfaWQiOm51bGwsImFnZW5jeV9pZCI6bnVsbCwiYWR2ZXJ0aXNlcl9pZCI6bnVsbH19XX0seyJjbGllbnQiOiJhZHNlcnZlciIsInJvbGVzIjpbeyJpZCI6MTQsIm5hbWUiOiJST0xFX0FETUlOIiwicmVzb3VyY2UiOnsiY2xpZW50X2lkIjpudWxsLCJhZ2VuY3lfaWQiOm51bGwsImFkdmVydGlzZXJfaWQiOm51bGx9fV19LHsiY2xpZW50IjoiZHNwIiwicm9sZXMiOlt7ImlkIjoyNCwibmFtZSI6IkRTUF9BRE1JTiIsInJlc291cmNlIjp7ImNsaWVudF9pZCI6bnVsbCwiYWdlbmN5X2lkIjpudWxsLCJhZHZlcnRpc2VyX2lkIjpudWxsfX1dfV0sImFjbCI6W119.ZJJnc2pLLuEhOnsOjImLW4CXE_MQFV4rOwxTcaGyU-8qDQpST_TDbqYrR2hppnoo9aSIB3Vtt2sgaHBbqT1cldyP1s6fY0m9ExIpARYqZlLxZaQODbkgV7td33Y5QyTQGA3OrwYqHZ453TtgdctSbdX55wjSAVAbxMgqWhxyRo2GfEkgHclah0-ykMSgr-h16D_Mbn6_zPANSRqNfFFR-xxNX0vAtixTtOSeUAVx5S7h-MWAHZxXkc6ljASWGpRUepiT2XUlC3cuwQlz07LGDZ6zqtISsPk4TxIvCeJUvpdsD2m9U4E9js7w_7H-vi478EJU_gyhJo95NVblutVUtQ',
        }
      );
      questor.send(request)
      .then((request) => {
        request.response.json().then((json) => {
          console.log('success', json);
          resolve(json);
        }).catch((err) => {
          console.log('error', err)
          reject(err);
        })
      })
      .catch((error) => reject(error));
  })
}