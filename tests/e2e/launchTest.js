const puppeteer = require('puppeteer');
const fs = require('fs');

fs.readdir('tests/e2e/tests', async (err, files) => {
  if (err) throw new Error(err);
  let browser;
  let page;

  let doneTest = 0;

  try {
    browser = await puppeteer.launch({
      headless: true,
      slowMo: 250,
    });
  } catch(e) {
    throw new Error(e);
  }

  try {
    page = await browser.newPage()
  } catch(e) {
    throw new Error(e);
  }
  
  // enable console.logging
  page.on('console', msg => console.log('PAGE LOG:', msg.args[0]._remoteObject.value));
  page.on('request', request => console.log('REQUEST LOG:', request));
  
  await page.addScriptTag({
    path: 'dist/index.min.js',
  });

  files.forEach(async (file) => {
    const test = require(`./tests/${file}`);

    await page.evaluate(test);
    doneTest++;
    if (doneTest === files.length) await browser.close();
    
  });
});