import test from 'ava';
import { expect } from 'chai';

const nock = require('nock');

import RESTRequest from '../../src/models/RESTRequest';
import QHeaders from '../../src/models/QHeaders';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import DataStore from '../../src/classes/DataStore';

const store = new DataStore({});
const jsonResponse = {
  message: 'fetched !',
};
const nockEndpoint = nock('https://test.com')
                    .get('/')
                    .reply(200, jsonResponse);

const defaultConfig = {
  protocol: new Protocol('graphql'),
  cacheConfig: new CacheConfiguration('cache', store),
  queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test.com'),
  headers: {},
};

test('should create RESTRequest', (t) => {
  const request = new RESTRequest(
    defaultConfig.protocol,
    defaultConfig.queryConfig,
    defaultConfig.cacheConfig,
    defaultConfig.headers,
  );
  expect(request).to.exist;
  expect(request).to.be.an.instanceOf(RESTRequest);
  expect(request.id).to.match(/.+-.+-.+-.+-.+/);
});

test('should send request to url', async (t) => {
  const request = new RESTRequest(
    defaultConfig.protocol,
    defaultConfig.queryConfig,
    defaultConfig.cacheConfig,
    defaultConfig.headers,
  );
  const response = await request.send();
  const json = await response.json();
  expect(json).to.eql(jsonResponse);
});