import test from 'ava';
import { expect } from 'chai';

import Body from '../../src/models/Body';

const testJson = {
  test: 'test',
  another: 0,
  another2: ['array'],
  obj: {
    test: '?',
  },
};

test('should apply keys to itself', (t) =>{
  const body = new Body(testJson);
  expect(body).to.eql(testJson);
});