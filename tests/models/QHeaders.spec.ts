import test from 'ava';
import { expect } from 'chai';

import headerGuard from '../../src/types/headerguard';
import QHeader from '../../src/models/QHeaders';


test('should create a new instance of QHeader', (t) => {
  const headers = new QHeader();
  expect(headers).to.exist;
  expect(headers).to.be.an.instanceof(QHeader);
  expect(headers.headers).to.eql({});
});

test('should add headers at creation if Array<[string, string]>', (t) => {
  const headers = new QHeader([
    ['Accept', '*'],
    ['Authorization', 'Bearer xxxxxxx'],
  ]);
  expect(headers.length).to.equal(2);
  expect(headers.headers).to.eql({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
});

test('should add headers at creation if Object with multiple keys', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
  expect(headers.length).to.equal(2);
  expect(headers.headers).to.eql({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
});

test('should throw error on invalid constructor arguments', (t) => {
  expect(() => new QHeader([
    {
      'Accept': 'test',
    }
  ])).to.throw(TypeError, 'invalid constructor arguments for QHeader, required Array<[string, string]> or [key: string]: any');
});

test('should add headers at creation if Object with 1 key', (t) => {
  const headers = new QHeader({
    'Accept': '*',
  });
  expect(headers.length).to.equal(1);
  expect(headers.headers).to.eql({
    'Accept': '*',
  });
});

test('should correctly set guard', (t) => {
  const headers = new QHeader();
  headers.guard = 'immutable';
  expect(headers.guard).to.equal('immutable');
  headers.guard = 'request';
  expect(headers.guard).to.equal('request');
  headers.guard = 'request-no-cors';
  expect(headers.guard).to.equal('request-no-cors');
  headers.guard = 'response';
  expect(headers.guard).to.equal('response');
  headers.guard = 'none';
  expect(headers.guard).to.equal('none');
});

test('should iterate', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
  const testValues = [];

  for (const key of headers) {
    testValues.push(key);
  }

  expect(testValues).to.eql([['Accept', '*'], ['Authorization', 'Bearer xxxxxxx']]);
});

test('should iterate over entries', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
  const testValues = [];

  for (const key of headers.entries()) {
    testValues.push(key);
  }

  expect(testValues).to.eql([['Accept', '*'], ['Authorization', 'Bearer xxxxxxx']]);
});

test('should iterate over keys', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
  const testValues = [];

  for (const key of headers.keys()) {
    testValues.push(key);
  }

  expect(testValues).to.eql(['Accept', 'Authorization']);
});

test('should iterate over values', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });
  const testValues = [];

  for (const key of headers.values()) {
    testValues.push(key);
  }

  expect(testValues).to.eql(['*', 'Bearer xxxxxxx']);
});

test('should get the next value', (t) => {
  const headers = new QHeader({
    'Accept': '*',
    'Authorization': 'Bearer xxxxxxx',
  });

  expect(headers.next()).to.eql({
    done: false,
    value: ['Accept', '*'],
  });
  expect(headers.next()).to.eql({
    done: false,
    value: ['Authorization', 'Bearer xxxxxxx'],
  });
  expect(headers.next()).to.eql({
    done: true,
    value: null,
  });
  expect(headers.next()).to.eql({
    done: true,
    value: null,
  });
});

test('should append the headers', (t) => {
  const headers = new QHeader();
  
  headers.append('Accept', 'value');

  expect(headers.headers).to.eql({
    'Accept': 'value'
  });
});

test('should set on append if header exists', (t) => {
  const headers = new QHeader({
    'Accept': 'test',
  });

  expect(headers.headers).to.eql({
    'Accept': 'test'
  })
  
  headers.append('Accept', 'value');

  expect(headers.headers).to.eql({
    'Accept': 'value'
  });
});

test('should throw an error on wrong header type', (t) => {
  const headers = new QHeader();
  expect(() => headers.append('test', 'value')).to.throw(TypeError, 'test is not a valid header');
});

test('should delete existing header', (t) => {
  const headers = new QHeader({
    'Accept': 'test',
  });

  expect(headers.headers).to.eql({
    'Accept': 'test'
  })
  
  headers.delete('Accept');

  expect(headers.headers).to.eql({});
  expect(headers.length).to.equal(0);
});

test('should throw error on delete header doesn not exist', (t) => {
  const headers = new QHeader({
    'Accept': 'test',
  });

  expect(() => headers.delete('test')).to.throw(ReferenceError, 'test is not a header');
});

test('should iterate on each element with keyId, MediaKeyStatus format', (t) => {
  const headers = new QHeader({
    'Accept': 'test',
    'content-type': 'application/json',
  });
  const testValues: Array<any> = [];

  headers.forEach((value, media) => {
    testValues.push({
      value,
      media,
    });
  });

  expect(testValues).to.eql([
    {
      value: ['Accept', 'test'],
      media: 'usable',
    },
    {
      value: ['Content-Type', 'application/json'],
      media: 'usable',
    }
  ]);
});

test('should return true if has header and false if not', (t) => {
  const headers = new QHeader({
    'Accept': 'test',
    'content-type': 'application/json',
  });

  expect(headers.has('accept')).to.be.true;
  expect(headers.has('Accept')).to.be.true;
  expect(headers.has('ACCEPT')).to.be.true;

  expect(headers.has('test')).to.be.false;
  expect(headers.has('Test')).to.be.false;
  expect(headers.has('TEST')).to.be.false;
});

test('should set the headers if it exists and push if not', (t) => {
  const headers = new QHeader();
  
  headers.set('Accept', 'value');

  expect(headers.headers).to.eql({
    'Accept': 'value'
  });

  headers.set('Accept', 'test');
  
  expect(headers.headers).to.eql({
    'Accept': 'test'
  });
});

test('should throw error on set if header name is not valid', (t) => {
  const headers = new QHeader();

  expect(() => headers.set('test', 'test')).to.throw(TypeError, 'test is not a valid header');
});