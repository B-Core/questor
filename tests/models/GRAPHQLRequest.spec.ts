import test from 'ava';
import { expect } from 'chai';

const nock = require('nock');

import GraphqlRequest from '../../src/models/GraphqlRequest';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import GraphqlQueryConfiguration from '../../src/models/GraphqlQueryConfiguration';
import DataStore from '../../src/classes/DataStore';

const store = new DataStore({});
const jsonResponse = {
  message: 'fetched !',
};
const nockEndpoint = nock('https://test.com')
                    .post('/')
                    .reply(200, jsonResponse);

const query = `query test($id: Int){
  user(id: $id) {
    firstName
    lastName
  }
}`;

const variables = {
  id: 5,
};

const defaultConfig = {
  protocol: new Protocol('graphql'),
  cacheConfig: new CacheConfiguration('cache', store),
  queryConfig: new GraphqlQueryConfiguration('query', 'simple', 'https://test.com', query, variables),
  headers: {},
};

const panQuery = `query QueryDiscoveryReport ($params: DiscoveryParams!) {
  discoveryReport (params: $params) {
    results {
      bidRequest
      deviceCity
    }
  }
}`;

const panVariables = {
  params: {
    interval: {
      start: "2017-11-06T23:00:00.000Z",
      end: "2017-12-06T23:00:00.000Z",
    },
    granularity: 'NONE',
    groupBy: ['DEVICE_CITY'],
  },
};

const panResponse = '{"data":{"discoveryReport":{"results":[{"bidRequest":83720,"deviceCity":"Marseille"}]}}}';

test('should create GraphqlRequest', (t) => {
  const request = new GraphqlRequest(
    defaultConfig.protocol,
    defaultConfig.queryConfig,
    defaultConfig.cacheConfig,
    defaultConfig.headers,
  );
  expect(request).to.exist;
  expect(request).to.be.an.instanceOf(GraphqlRequest);
  expect(request.id).to.match(/.+-.+-.+-.+-.+/);
});

test('should send request to url', async (t) => {
  const request = new GraphqlRequest(
    defaultConfig.protocol,
    defaultConfig.queryConfig,
    defaultConfig.cacheConfig,
    defaultConfig.headers,
  );
  const response = await request.send();
  const json = await response.json();
  expect(json).to.eql(jsonResponse);
});

// test('test on pan-server', async (t) => {
//   defaultConfig.headers.append('Authorization', 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJuYW1lIjoiU3VwZXIgQWRtaW4iLCJlbWFpbCI6InN1cGVyYWRtaW5AczRtLmlvIiwiaXNzIjoiaHR0cDpcL1wvcHJvamVjdC1zc28uZGV2Iiwic3ViIjoiczRtLWZ1bGxzdGFja3w6aWQzIiwiYXVkIjp7Im5hbWUiOiJTU08iLCJpZGVudGlmaWVyIjoic3NvIiwiaG9zdCI6Im1hcmF0aG9uLWxiLm1hcmF0aG9uLm1lc29zLmRldiJ9LCJleHAiOjE1MTI2ODEyNzcsIm5iZiI6MTUxMjYzODA3NywiaWF0IjoxNTEyNjM4MDc3LCJqdGkiOiIzNWJmY2Q4YTkzZTE0NjA3NTY5Y2VkMDc4Mjg3ZTgwOTU3NDYxZTJkIiwicGVybWlzc2lvbnMiOnsibG9jYWwtYXBpIjp7ImNsaWVudCI6eyJuYW1lIjoiRnVzaW8iLCJpZGVudGlmaWVyIjoibG9jYWwtYXBpIiwiaG9zdCI6ImxvY2FsLWFkdHJhY2tlcnVpLnNhbTRtLmNvbSJ9LCJyb2xlcyI6W3sicm9sZV9pZCI6Mywicm9sZV9uYW1lIjoiUk9MRV9BRE1JTiIsImNsaWVudF9pZCI6bnVsbCwiYWdlbmN5X2lkIjpudWxsLCJhZHZlcnRpc2VyX2lkIjpudWxsLCJhY2wiOm51bGx9XX0sImRzcCI6eyJjbGllbnQiOnsibmFtZSI6IkRTUCIsImlkZW50aWZpZXIiOiJkc3AiLCJob3N0IjoiLm1hcmF0aG9uLm1lc29zLmRldiJ9LCJyb2xlcyI6W3sicm9sZV9pZCI6MTMsInJvbGVfbmFtZSI6IkRTUF9BRE1JTiIsImNsaWVudF9pZCI6bnVsbCwiYWdlbmN5X2lkIjpudWxsLCJhZHZlcnRpc2VyX2lkIjpudWxsLCJhY2wiOm51bGx9XX19fQ.CV0ro_FY9_mi8gQrLP9r60Uj_0BtEu7RobGlFE9JyLqqt-Cm_KaFREkvo_fuYMfSthOJScuNUkBLPKH9XmWb14OVbltCwobN9pLq0My54dsApWcvYlFPQN0raacmTHzOdIWSnjmQkARAsZWGS3DLVlq59bjTBcoZekOldRqxr6ARFJ_Y0yEK7uVpot3JsmYwwR_v-hoRy2-tGqoKnnOevqrEU99Dp1kiNmYGttdqvJZZ4d3HUEXw4NmYdFTjwrD3RtUjX23uHWLc8R1aRO2Ztz0sa2KjJZHruIEREfSlSIUC1BlP_ToKX50uGocz5pltOb7j_obTRKpsXYmapN5Cgg');
//   const queryConfig = new GraphqlQueryConfiguration('query', 'simple', 'http://marathon-lb.marathon.mesos.dev:31014/graphql', panQuery, panVariables);
//   const request = new GraphqlRequest(
//     defaultConfig.protocol,
//     queryConfig,
//     defaultConfig.cacheConfig,
//     defaultConfig.headers,
//   );
//   const response = await request.send();
//   const json = await response.json();
//   expect(JSON.stringify(json)).to.equal(panResponse);
// });