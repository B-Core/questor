import test from 'ava';
import { expect } from 'chai';

// @ts-ignore
import { findIndex } from 'lodash-es/array';

import { module, actionModule } from '../../src/types/module';
import Protocol from '../../src/classes/Protocol';
import CacheConfiguration from '../../src/classes/CacheConfiguration';
import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import Body from '../../src/models/Body';
import QHeaders from '../../src/models/QHeaders';
import RESTRequest from '../../src/models/RESTRequest';
import { CacheStore, dataHasKeys, checkDataWithReference } from '../../src/models/CacheStore';
import DataStore from '../../src/classes/DataStore';

let store: CacheStore;
let request: RESTRequest;

const defaultConfig = {
  store,
  protocol: new Protocol('graphql'),
  cacheConfig: new CacheConfiguration('cache', store),
  queryConfig: new RESTQueryConfiguration('GET', 'simple', 'http://test.com'),
  headers: {},
};

test.beforeEach((t) => {
  store = new CacheStore({});
  request = new RESTRequest(defaultConfig.protocol,defaultConfig.queryConfig, defaultConfig.cacheConfig, defaultConfig.headers);
});

test('dataHasKey: should return an empty array if the data has all the keys', (t) => {
  const data1 = {
    id: 'test-id-1',
    name: 'test',
  };
  expect(dataHasKeys(data1, ['id'])).to.have.lengthOf(0);
});

test('dataHasKey: should return an array with the different keys if the data does\'nt have the key', (t) => {
  const data1 = {
    id: 'test-id-1',
    name: 'test',
  };
  const testData = dataHasKeys(data1, ['id','name', 'test']);
  expect(testData).to.have.lengthOf(1);
  expect(testData).to.contain('test');
});

test('checkDataWithReference: should return nothin if there is not difference', (t) => {
  const data1 = {
    id: 'test-id-1',
    name: 'test',
  };
  expect(checkDataWithReference(data1, ['id'])).not.to.equal(`${data1} does not contain searchKey id`);
});

test('checkDataWithReference: should return return error if there is a difference', (t) => {
  const data1 = {
    id: 'test-id-1',
    name: 'test',
  };
  const testData = checkDataWithReference(data1, ['test']);
  const error = new Error(`${JSON.stringify(data1)} does not contain searchKey test`);
  // @ts-ignore
  expect(testData.message).to.equal(error.message);
  expect(testData).to.be.an.instanceOf(Error);
});

test('should correctly instanciate a CacheStore', (t) => {
  expect(store).to.exist;
  expect(store).to.be.an.instanceof(CacheStore);
});

test('should use default store state', (t) => {
  const st = new CacheStore({});
  expect(st.state).to.eql({
    pending: {},
    resolved: {},
    failed: {},
  });
});

test('should get the searchKey based on referenceKey', (t) => {
  expect(store.getSearchKeys()).to.eql(['id']);
  store.referenceKey = 'test';
  expect(store.getSearchKeys()).to.eql(['test']);
});

test('should correctly commit the data', (t) => {
  store.commit('set:request-failed', [], request);
  expect(store.state.failed[request.id]).to.eql(request);
  store.commit('set:request-pending', [], request);
  expect(store.state.pending[request.id]).to.eql(request);
  store.commit('set:request-resolved', [], request);
  expect(store.state.resolved[request.id]).to.eql(request);
});

test('should pass correct arguments to mutation', (t) => {
  store.mutations.testMutation = (state: module, request: Request, searchKey: Array<string>): void => {
    expect(state).to.eql(store.state);
    expect(request).to.eql(request);
    expect(searchKey).to.eql(['id']);
  };
  store.commit('testMutation', [], request);

  store.mutations.testMutation2 = (state: module, request: Request, searchKey: Array<string>): void => {
    expect(state).to.eql(store.state);
    expect(request).to.eql(request);
    expect(searchKey).to.eql(['id', 'name']);
  };
  store.commit('testMutation2', ['name'], request);
});

test('should correctly delete the data', (t) => {
  store.commit('set:request-failed', [], request);
  store.commit('set:request-pending', [], request);
  store.commit('set:request-resolved', [], request);

  store.commit('delete:request-failed', [], request);
  expect(store.state.failed[request.id]).to.not.exist;
  store.commit('delete:request-pending', [], request);
  expect(store.state.pending[request.id]).to.not.exist;
  store.commit('delete:request-resolved', [], request);
  expect(store.state.resolved[request.id]).to.not.exist;
});

test('should pass correct arguments to actions', (t) => {
  store.actions.testAction = (state: module, commit: Function, searchKey: Array<string>, request: Request): void => {
    expect(state).to.eql(store.state);
    expect(commit).to.eql(store.commit);
    expect(request).to.eql(request);
    expect(searchKey).to.eql(['id']);
  };
  store.dispatch('testAction', [], request);

  store.actions.testAction2 = (state: module, commit: Function, searchKey: Array<string>, request: Request): void => {
    expect(state).to.eql(store.state);
    expect(commit).to.eql(store.commit);
    expect(request).to.eql(request);
    expect(searchKey).to.eql(['id', 'name']);
  };
  store.dispatch('testAction2', ['name'], request);
});