import test from 'ava';
import { expect } from 'chai';

import { pollingStates, pollingState } from '../../src/types/polling';

import GraphqlQueryConfiguration from '../../src/models/GraphqlQueryConfiguration';

const defaultPollingStates:pollingStates = {
  success: ['STATE', ['SUCCESS']],
  error: ['STATE', ['ERROR']],
  running: ['STATE', ['RUNNING']],
  initialized: ['STATE', ['INITIALIZED']],
};

const newPollingStates:pollingStates = {
  success: ['TEST', ['SUCCESS']],
  error: ['TEST', ['ERROR']],
  running: ['TEST', ['RUNNING']],
  initialized: ['TEST', ['INITIALIZED']],
};

const query = `query test($id: Int){
  user(id: $id) {
    firstName
    lastName
  }
}`;

const variables = {
  id: 5,
};

test('should set query and variables', (t) => {
  const graphConf = new GraphqlQueryConfiguration('query', 'simple', 'https://test.com', query, variables);
  expect(graphConf.query).to.equal(query);
  expect(graphConf.variables).to.equal(variables);
  expect(graphConf.definition).to.exist;
});

test('should not set pollingStates', (t) => {
  const graphConf = new GraphqlQueryConfiguration('query', 'simple', 'https://test.com', query, variables);
  expect(graphConf.pollingStates).to.eql(defaultPollingStates);
});

test('should set pollingStates', (t) => {
  const graphConf = new GraphqlQueryConfiguration('query', 'simple', 'https://test.com', query, variables, newPollingStates);
  expect(graphConf.pollingStates).to.eql(newPollingStates);
});