import test from 'ava';
import { expect } from 'chai';

import { pollingStates, pollingState } from '../../src/types/polling';

import RESTQueryConfiguration from '../../src/models/RESTQueryConfiguration';
import Body from '../../src/models/Body';

const defaultPollingStates:pollingStates = {
  success: ['STATE', ['SUCCESS']],
  error: ['STATE', ['ERROR']],
  running: ['STATE', ['RUNNING']],
  initialized: ['STATE', ['INITIALIZED']],
};

const newPollingStates:pollingStates = {
  success: ['TEST', ['SUCCESS']],
  error: ['TEST', ['ERROR']],
  running: ['TEST', ['RUNNING']],
  initialized: ['TEST', ['INITIALIZED']],
};

test('should not set pollingStates', (t) => {
  const graphConf = new RESTQueryConfiguration('GET', 'simple', 'https://test.com');
  expect(graphConf.pollingStates).to.eql(defaultPollingStates);
});

test('should set pollingStates', (t) => {
  const graphConf = new RESTQueryConfiguration('GET', 'simple', 'https://test.com', new Body(), newPollingStates);
  expect(graphConf.pollingStates).to.eql(newPollingStates);
});