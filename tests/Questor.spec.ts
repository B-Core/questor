import test from 'ava';
import { expect } from 'chai';
const nock = require('nock');

import Questor from '../src/Questor';
import Protocol from '../src/classes/Protocol';
import CacheConfiguration from '../src/classes/CacheConfiguration';
import Body from '../src/models/Body';
import RESTQueryConfiguration from '../src/models/RESTQueryConfiguration';
import RESTRequest from '../src/models/RESTRequest';
import GraphqlQueryConfiguration from '../src/models/GraphqlQueryConfiguration';
import GraphqlRequest from '../src/models/GraphqlRequest';
import { Request } from '../src/types/Request';
import DataStore from '../src/classes/DataStore';

let questor: Questor;

const jsonResponse = {
  message: 'fetched !',
};

const nockEndpoint = nock('https://test.com')
                    .get('/')
                    .times(3)
                    .reply(200, jsonResponse);

test.beforeEach((t) => {
  questor = new Questor({});
})

test('should create a new instance of Questor', (t) => {
  expect(questor).to.exist;
  expect(questor).to.be.an.instanceof(Questor);
});

test('should create a new Request with default parameters', (t) => {
  const request = questor.createRequest();
  expect(request).to.be.an.instanceOf(GraphqlRequest);

  expect(request.protocol.type).to.equal('GRAPHQL');

  expect(request.queryConfig.type).to.equal('SIMPLE');
  expect(request.queryConfig.method).to.equal('query');
  expect(request.queryConfig.url).to.equal('https://test.com');

  expect(request.cacheConfig.type).to.equal('CACHE');
  expect(request.cacheConfig.store).to.eql(new DataStore({}));

  expect(request.headers.get('accept')).to.eql('application/json');
  expect(request.headers.get('content-type')).to.eql('application/json');
});


test('should create a new GraphqlRequest', (t) => {
  const protocol = new Protocol('graphql');
  const queryConfig = new GraphqlQueryConfiguration('mutation', 'polling', 'https://test.com');
  const request = questor.createRequest(protocol, queryConfig);
  expect(request).to.be.an.instanceOf(GraphqlRequest);

  expect(request.protocol.type).to.equal('GRAPHQL');

  expect(request.queryConfig.type).to.equal('POLLING');
  expect(request.queryConfig.method).to.equal('mutation');
  expect(request.queryConfig.url).to.equal('https://test.com');

  expect(request.cacheConfig.type).to.equal('CACHE');
  expect(request.cacheConfig.store).to.eql(new DataStore({}));

  expect(request.headers.get('accept')).to.eql('application/json');
  expect(request.headers.get('content-type')).to.eql('application/json');
});

test('should create a new RESTRequest', (t) => {
  const protocol = new Protocol('rest');
  const queryConfig = new RESTQueryConfiguration('GET', 'simple', 'https://test.com');
  const request = questor.createRequest(protocol, queryConfig);
  expect(request).to.be.an.instanceOf(RESTRequest);

  expect(request.protocol.type).to.equal('REST');

  expect(request.queryConfig.type).to.equal('SIMPLE');
  expect(request.queryConfig.method).to.equal('GET');
  expect(request.queryConfig.url).to.equal('https://test.com');

  expect(request.cacheConfig.type).to.equal('CACHE');
  expect(request.cacheConfig.store).to.eql(new DataStore({}));

  expect(request.headers.get('accept')).to.be.null;
  expect(request.headers.get('content-type')).to.be.null;
});

test('should throw an error on invalid protocol', (t) => {
  const queryConfig = new RESTQueryConfiguration('GET', 'simple', 'https://test.com');

  expect(() => questor.createRequest(({ type: 'test' } as Protocol), queryConfig)).to.throw(TypeError, 'test is not a known protocol');
});

test('should throw an error if the queryConfig does\'nt match the protocol asked', (t) => {
  const protocol = new Protocol('rest');
  const queryConfig = new GraphqlQueryConfiguration('mutation', 'polling', 'https://test.com');
  expect(() => questor.createRequest(protocol, queryConfig)).to.throw(TypeError, 'queryConfig is not a RESTQueryConfiguration but GraphqlQueryConfiguration');

  const protocol2 = new Protocol('graphql');
  const queryConfig2 = new RESTQueryConfiguration('GET', 'simple', 'https://test.com');
  expect(() => questor.createRequest(protocol2, queryConfig2)).to.throw(TypeError, 'queryConfig is not a GraphqlQueryConfiguration but RESTQueryConfiguration');
});

test('should create request and add it to pending', (t) => {
  const protocol = new Protocol('rest');
  const queryConfig = new RESTQueryConfiguration('GET', 'simple', 'https://test.com');
  const request = questor.createRequest(protocol, queryConfig);
  questor.add(request);
  expect(Object.keys(questor.queryManager._store.state.pending)).to.have.lengthOf(1);
});

test('should add request to resolved if request resolved', async (t) => {

  questor = new Questor({
    protocol: new Protocol('rest'),
    cacheConfig: new CacheConfiguration('no-cache', new DataStore({})),
    queryConfig: new RESTQueryConfiguration('GET', 'simple', 'https://test.com'),
  });
  
  for (let i = 0; i < 3; i++) {
    questor.add(questor.createRequest());
  }

  expect(questor.queryManager.pendingRequests).to.have.lengthOf(3);
  const requests = await questor.sendAll();
  expect(requests).to.have.lengthOf(3);
  expect(questor.queryManager.pendingRequests).to.have.lengthOf(0);

  await setTimeout(() => {
    requests.forEach(async (request: Request) => expect(await request.response.json()).to.eql(jsonResponse));
    expect(questor.queryManager.resolvedRequests).to.have.lengthOf(3);
  }, 1000);
});

test('should create a valid cacheConfiguration', (t) => {
  const cacheConfig = Questor.createCacheConfiguration({
    type: 'cache',
    optimisticResponse: {"data":{}}
  });
  expect(cacheConfig).to.be.instanceof(CacheConfiguration);
  expect(cacheConfig.type).to.equal('CACHE');
  expect(cacheConfig.optimisticResponse).to.eql({"data":{}});
});

test('should create a valid GRAPHQLQueryConfiguration', (t) => {
  const queryConfig = Questor.createQueryConfiguration('graphql', {
    method: 'query',
    type: 'simple',
  }) as GraphqlQueryConfiguration;
  expect(queryConfig).to.be.instanceof(GraphqlQueryConfiguration);
  expect(queryConfig.method).to.equal('query');
  expect(queryConfig.type).to.eql('SIMPLE');
});

test('should create a valid RESTQueryConfiguration', (t) => {
  const queryConfig = Questor.createQueryConfiguration('rest', {
    type: 'simple',
  }) as RESTQueryConfiguration;
  expect(queryConfig).to.be.instanceof(RESTQueryConfiguration);
  expect(queryConfig.method).to.equal('GET');
  expect(queryConfig.type).to.eql('SIMPLE');
});

test('should throw an error if type is not valid', (t) => {
  expect(() => Questor.createQueryConfiguration('test', {})).to.throw(TypeError, 'invalid type for configuration: test');
});