// rollup.config.js
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import alias from 'rollup-plugin-alias';
import typescript from 'rollup-plugin-typescript';

const substituteModulePaths = {};

export default {
    sourcemap: true,
    format: 'es',
    plugins: [
      alias(substituteModulePaths),
      nodeResolve(),
      commonjs({
        namedExports: {
          './node_modules/uuid': ['v1', 'v4', 'v5'],
          './node_modules/chai': ['expect'],
        }
      }),
      typescript({
        typescript: require('typescript'),
      }),
    ]
}