// rollup.config.js
import commonjs from 'rollup-plugin-commonjs';
import nodeResolve from 'rollup-plugin-node-resolve';
import alias from 'rollup-plugin-alias';
import typescript from 'rollup-plugin-typescript';

const substituteModulePaths = {};

export default {
    input: 'src/Questor.ts',
    name: 'Questor',
    sourcemap: true,
    output: [
      {
        file: 'dist/index.js',
        format: 'iife',
      },
      {
        file: 'dist/index.cjs.js',
        format: 'cjs',
      },
      {
        file: 'dist/index.es.js',
        format: 'es',
      },
      {
        file: 'dist/index.js',
        format: 'umd',
      },
    ],
    plugins: [
        alias(substituteModulePaths),
        nodeResolve({
          jsnext: true,
          main: true,
          browser: true,
        }),
        commonjs({
          namedExports: {
            './node_modules/uuid': ['v1', 'v4', 'v5'],
            './node_modules/lodash': ['defaults', 'merge', 'find', 'findIndex', 'difference', 'cloneDeep'],
          }
        }),
        typescript({
          typescript: require('typescript'),
        }),
    ]
}