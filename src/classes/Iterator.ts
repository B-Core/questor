/**
 * Define the Iterator object
 * required by ES6 Header class
 * Define an IterableIterator []()
 * 
 * @export default
 * @class Iterator
 * @implements {IterableIterator<any>}
 */

export default class Iterator implements IterableIterator<any> {
  protected _list: Array<any> = [];
  protected _currentIndex: number;

  constructor(values: Array<any>) {
    this._list = values;
    this._currentIndex = 0;
  }
  
  [Symbol.iterator](): IterableIterator<any> {
    return this;
  }

  public next(): IteratorResult<any> {
    const current = this._list[this._currentIndex];
    let iterator = {
      done: false,
      value: current,
    };
    this._currentIndex += this._currentIndex <= this._list.length ? 1 : 0;
    if (this._currentIndex > this._list.length) {
      iterator.done = true;
      iterator.value = null;
    }
    return iterator;
  }
}