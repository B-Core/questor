import { module, actionModule } from '../types/module';

export interface DataStoreArguments {
  state?: module;
  mutations?: actionModule;
  actions?: actionModule;
  getters?: actionModule;
}

/**
 * Define the DataStore class
 * It is succinct implementation on the Redux pattern
 * handles mutations, getters and asynchronous actions with state
 * 
 * @export default
 * @class DataStore
 */

export default class DataStore {
  
  state: module;
  mutations: actionModule;
  actions: actionModule;
  getters: actionModule;

  constructor({
    state = {},
    mutations = {},
    actions = {},
    getters = {},
  }: DataStoreArguments) {
    this.state = state;
    this.mutations = mutations;
    this.actions = actions;
    this.getters = getters;
  }

  commit(mutationName: string, ...args: Array<any>): any {
    return this.trigger('mutation', mutationName, [...args]);
  }

  dispatch(actionName: string, ...args: Array<any>): Promise<any> {
    return this.trigger('action', actionName, [this.commit.bind(this), ...args]);
  }

  get(actionName: string, ...args: Array<any>): any {
    return this.trigger('getter', actionName, [this.get.bind(this), ...args]);
  }

  trigger(type: string, actionName:string, args: Array<any>):any {
    switch(type) {
      case 'mutation': {
        if (!this.mutations[actionName]) throw new ReferenceError(`no mutation ${actionName}`);
        return this.mutations[actionName].apply(this, [this.state, ...args]);
      }
      case 'action': {
        if (!this.actions[actionName]) throw new ReferenceError(`no action ${actionName}`);
        return this.actions[actionName].apply(this, [this.state, ...args]);
      }
      case 'getter': {
        if (!this.getters[actionName]) throw new ReferenceError(`no getter ${actionName}`);
        return this.getters[actionName].apply(this, [this.state, ...args]);
      }
    }
  }

}