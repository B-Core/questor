import protocols from '../static/protocols';

/**
 * Define the Protocol class to use with every Request
 * 
 * @export default
 * @class Protocol
 */

export default class Protocol {
  
  type: string;

  constructor(type: string) {
    if (!type) throw new Error('type must be defined');
    const tType = type.toUpperCase();
    if (tType in protocols) this.type = tType;
    else throw new TypeError(`Invalid Protocol type: ${type}`);
  }
}