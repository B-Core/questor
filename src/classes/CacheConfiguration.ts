import cacheTypes from '../static/cacheTypes';
import DataStore from './DataStore';

/**
 * Define the CacheConfiguration for all the Request
 * The CacheConfiguration stock all the data needed to correctly update the client store
 * 
 * @export default
 * @class CacheConfiguration
 */

export default class CacheConfiguration {

  type: string;
  store: DataStore;
  uniqueKeys?: Array<string>;
  storeModuleName?: string;
  optimisticResponse?: object;
  successCallback?: Function;
  errorCallback?: Function;
  applied: Boolean;
  new: Boolean;
  backup: any;

  constructor(
    type: string,
    store: DataStore,
    uniqueKeys: Array<string> = ['id'],
    storeModuleName: string = 'data', 
    optimisticResponse?: object,
    successCallback?: Function,
    errorCallback?: Function,
  ) {
    if (!type) throw new Error('type must be defined');
    const tType = type.toUpperCase();
    if (tType in cacheTypes) this.type = tType;
    else throw new TypeError(`Invalid CacheConfiguration type: ${type}`);
    
    this.applied = false;

    this.store = store;
    this.uniqueKeys = uniqueKeys;
    this.storeModuleName = storeModuleName;
    this.optimisticResponse = optimisticResponse;
    this.successCallback = successCallback;
    this.errorCallback = errorCallback;
  }
}