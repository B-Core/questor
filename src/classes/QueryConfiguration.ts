// @ts-ignore
import { defaults } from 'lodash';
import queryTypes from '../static/queryTypes';
import { pollingStates, pollingState } from '../types/polling';

/**
  @hidden
*/
const defaultPollingStates:pollingStates = {
  success: ['STATE', ['SUCCESS']],
  error: ['STATE', ['ERROR']],
  running: ['STATE', ['RUNNING']],
  initialized: ['STATE', ['INITIALIZED']],
};

/**
 * Check if a given object is a polling state
 * 
 * @function isPollingState
 * @param {any} o - the argument to check
 * @return {Boolean} - is a pollingState 
 */

export const isPollingState = (o:object): boolean => o instanceof Array && o[0] && o[1] && o[1] instanceof Array && o[1].length >= 1;


/**
 * Define the base QueryConfiguration object
 * 
 * @export QueryConfiguration
 * @class QueryConfiguration
 */

export class QueryConfiguration {
  
  type:string;
  url:string;
  pollingStates:pollingStates

  constructor(
    type:string,
    url: string,
    pollingStates:pollingStates = defaultPollingStates
  ) {
    if (!type) throw new Error('type must be defined');
    const tType = type.toUpperCase();
    if (tType in queryTypes) this.type = tType;
    else throw new TypeError(`Invalid QueryConfiguration type: ${type}`);

    this.url = url;

    this.pollingStates = defaults(pollingStates, defaultPollingStates);
    if (!isPollingState(this.pollingStates.success)) throw new Error('pollingStates success should be defined correctly')
    if (!isPollingState(this.pollingStates.error)) throw new Error('pollingStates error should be defined correctly')
  }
}