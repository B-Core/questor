/**
 * Define the RESTMethod type
 * 
 * @export RESTMethod
 * @type {RESTMethod}
 */
export type RESTMethod = 'GET' | 'POST' | 'PUT' | 'PATCH' | 'DELETE' | 'OPTION';