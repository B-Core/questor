/**
 * Define the pollingState type
 * 
 * @export pollingState
 * @type {pollingState}
 */

export type pollingState = [string, Array<string>]

/**
 * Defined the pollingStates type
 * 
 * @export pollingStates
 * @type {pollingStates}
 */

export type pollingStates = {
  success: pollingState,
  error: pollingState,
  running?: pollingState,
  initialized?: pollingState,
  [key: string]: any,
}