import { QueryConfiguration } from '../classes/QueryConfiguration';
import RESTQueryConfiguration from '../models/RESTQueryConfiguration';
import GraphqlQueryConfiguration from '../models/GraphqlQueryConfiguration';

/**
 * Define the QueryConfig type
 *
 * @export QueryConfig
 * @type {QueryConfig} 
 */

export type QueryConfig = QueryConfiguration | RESTQueryConfiguration | GraphqlQueryConfiguration;