/**
 * Defined the headerGuard type used in QHeader Class
 * 
 * @export default
 * @type {headerGuard}
 */

type headerGuard = 'immutable' | 'request' | 'request-no-cors' | 'response' | 'none';

export default headerGuard