/**
 * Typeguard ensuring that a given argument is an Object
 * 
 * @export isObject
 * @param {any} o - the arg to check
 * @return {Boolean} is an Object 
 */


const isObject = (o: any): o is { [key:string]: string } => {
  return !(o instanceof Function) && !(o instanceof Array);
}

export default isObject;