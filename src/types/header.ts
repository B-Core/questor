import { requestTypes } from '../static/requestTypes';

/**
 * Define the type for an individual requestType key
 * 
 * @type {requestTypeKey}
 */

export type requestTypeKey = keyof typeof requestTypes;

/**
 * Define the header type
 * 
 * @type {header}
 */

export type header = [requestTypeKey, string];

/**
 * Typeguard to define if a given argument is considered a header
 * 
 * @type {header}
 */

export const isHeader = (o: any): o is header => o instanceof Array && o[0] in requestTypes && typeof o[1] === 'string';

/**
 * Define what arguments are considered valid for a header
 * 
 * @export headerArg
 * @type {headerArg}
 */

export type headerArg = {
  [key:string]: any
}
/**
  @hidden
*/
const headerKeyReg = new RegExp(/(-[a-z])+/g);

/**
 * Check if a given string is a requestTypeKey and converts it
 * 
 * @export convertToHeaderKey
 * @func convertToHeaderKey
 * @param {string} str - the key to convert a check if is is a header key
 * @return {string} converted string
 */

export const convertToHeaderKey = (str: string): string => {
  const matches = str.match(headerKeyReg);
  let s = `${str[0].toUpperCase()}${str.slice(1)}`;
  if (matches !== null) {
    matches.forEach((match) => {
      s = s.replace(match, `${match[0]}${match[1].toUpperCase()}`);
    });
  }
  return s;
}

export default header;