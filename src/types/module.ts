/**
 * Define the store module type (which are basically Objects, but you know...types and all)
 * 
 * @export module
 * @type {module}
 */

export type module = {
  [key: string]: any;
}

/**
 * Define the store action module type
 * 
 * @export actionModule
 * @type {actionModule}
 */

export type actionModule = {
  [key: string]: Function;
}