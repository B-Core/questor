/**
 * Define the GraphqlMethod type
 * 
 * @export GraphqlMethod
 * @type {GraphqlMethod}
 */

export type GraphqlMethod = 'query' | 'mutation';