import RESTRequest from '../models/RESTRequest';
import GraphqlRequest from '../models/GraphqlRequest';
import RESTQueryConfiguration from '../models/RESTQueryConfiguration';

/**
 * Define the Request type
 * 
 * @export Request
 * @type {Request}
 */

export type Request = RESTRequest | GraphqlRequest;

/**
 * Typeguard to ensure a given argument is a REST Request 
 * but with no 'GET' protocol and no optimisticResponse
 * 
 * @export isRestNoGet
 * @type {isRestNoGet}
 */

export const isRestNoGet = (req: any): req is RESTRequest => req.queryConfig instanceof RESTQueryConfiguration && req.queryConfig.method !== 'GET' && !req.cacheConfig.optimisticResponse;