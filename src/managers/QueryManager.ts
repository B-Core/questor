// @ts-ignore
import { findIndex } from 'lodash';
// @ts-ignore
import { cloneDeep } from 'lodash';

import Request from '../interfaces/Request';
import { CacheStore } from "../models/CacheStore";
import RESTRequest from '../models/RESTRequest';
import RESTQueryConfiguration from '../models/RESTQueryConfiguration';
import GraphqlQueryConfiguration from '../models/GraphqlQueryConfiguration';
import { module } from '../types/module';

const checkPoll = (request: Request, json: any, pollState:string, p: any, c: any, ci: any, src: Boolean) => src = src !== false ? json[request.queryConfig.pollingStates[pollState][0]].includes(c) : false;


/**
 * Define the QueryManager class
 * It handles all the Request sending, polling and calls it's instance of CacheStore
 * when update is needed
 * 
 * @export default
 * @class QueryManager
 */

export default class QueryManager {

  _store: CacheStore;
  pollLimit: number;
  pollTime: number;

  constructor(
    store: CacheStore = new CacheStore({}),
    pollLimit: number = 200,
    pollTime: number = 300,
  ) {
    this._store = store;
    this.pollLimit = pollLimit;
    this.pollTime = pollTime;
  }

  add(...requests: Array<Request>): void {
    requests.forEach(request => this._store.commit('set:request-pending', [], request));
  }

  setRequestFailed(request: Request, reject: Function): void {
    this._store.commit('delete:request-pending', [], request);
    this._store.commit('delete:request-resolved', [], request);
    request.status = 'FAILED';
    if (request.cacheConfig.type === 'CACHE') this._store.updateClientCache(request);

    this._store.commit('set:request-failed', [], request);
    reject(request);
  }

  setRequestResolved(request: Request, resolve: Function): void {
    this._store.commit('delete:request-pending', [], request);
    this._store.commit('delete:request-failed', [], request);
    request.status = 'RESOLVED';
    if (request.cacheConfig.type === 'CACHE') this._store.updateClientCache(request);

    this._store.commit('set:request-resolved', [], request);
    resolve(request);
  }

  updateCache(response: Response, request: Request, resolve: Function, reject: Function): void {  
      request.response = response;
      if (response && (response.status >= 300 || request.status === 'FAILED')) {
        this.setRequestFailed(request, reject);
      } else if (response && (response.status >= 200 || request.status === 'RESOLVED')) {
        this.setRequestResolved(request, resolve);
      } else if (request.status && (request.status !== 'RUNNING' || request.status !== 'INITIALIZED')) {
        this.setRequestFailed(request, reject);
      }
  }

  async checkPollingState(request:Request, response: Response): Promise<any> {
    let json: module;
    let ret = false;
    try {
      json = await response.json();
    } catch(e) { } finally {
      if (json) {
        if (request.queryConfig.pollingStates.success[1].reduce(checkPoll.bind(this, request, json, 'success'), true)) {
          request.status = 'RESOLVED';
          ret = true;
        }
        else if (request.queryConfig.pollingStates.error[1].reduce(checkPoll.bind(this, request, json, 'error'), true)) {
          request.status = 'FAILED';
          ret = true;
        }
        else if (request.queryConfig.pollingStates.initialized[1].reduce(checkPoll.bind(this, request, json, 'initialized'), true)) {
          request.status = 'INITIALIZED';
          ret = false;
        }
        else if (request.queryConfig.pollingStates.running[1].reduce(checkPoll.bind(this, request, json, 'running'), true)) {
          request.status = 'RUNNING';
          ret = false;
        }
      }
      return ret;
    }
  }

  async poll(request: Request): Promise<any> {
    const parent = this;
    let interval: any;
    let count = 0;
    const poll = (resolve: Function, reject: Function) => {
      count++;
      if (count < this.pollLimit) {
        request.send()
        .then(async (response: Response) => {
          const pollState = await this.checkPollingState(request, response);
          if(pollState) {
            resolve(request);
            clearInterval(interval);
          }
        })
        .catch(async (response: Response) => {
          const pollState = await this.checkPollingState(request, response);
          reject(request);
          clearInterval(interval);
        });
      } else {
        request.status = 'FAILED';
        console.warn(`request ${request.id} failed because of poll limit`);
        reject(request);
        clearInterval(interval);
      }
    };
    return new Promise((resolve, reject) => {
      interval = setInterval(poll.bind(parent, resolve, reject), this.pollTime);
    });
  }

  execute(request: Request): Promise<any> {
    this._store.commit('set:request-pending', [], request);
    if (request.cacheConfig.type === 'CACHE') this._store.updateClientCache(request);
    return new Promise((resolve, reject) => {
      switch(request.queryConfig.type) {
        case 'SIMPLE': {
          request.send()
          .then((response: Response) => this.updateCache(response, request, resolve, reject))
          .catch((response: Response) => this.updateCache(response, request, resolve, reject));
          break;
        }
        case 'POLLING': {
          this.poll(request)
          .then((response: Response) => this.updateCache(response, request, resolve, reject))
          .catch((response: Response) => this.updateCache(response, request, resolve, reject));
          break;
        }
      }
    });
  }

  executeAll(): Promise<any> {
    return new Promise((resolve, reject) => {
      const resolved: Array<Request> = [];
      const requests = this.pendingRequests.concat(this.failedRequests);
      requests.forEach((request: Request) => this.execute(request)
      .then((response: Response) => {
        resolved.push(request);
        if (resolved.length === requests.length) resolve(requests);
      })
      .catch((error: Response) => {
        resolved.push(request);
        if (resolved.length === requests.length) resolve(requests);
      }));
    });
  }

  get pendingRequests() {
    return Object.keys(this._store.state.pending).map(key => this._store.state.pending[key]);
  }
  get failedRequests() {
    return Object.keys(this._store.state.failed).map(key => this._store.state.failed[key]);
  }
  get resolvedRequests() {
    return Object.keys(this._store.state.resolved).map(key => this._store.state.resolved[key]);
  }

}