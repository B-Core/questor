// @ts-ignore
import { difference, findIndex } from 'lodash';
// @ts-ignore
import { cloneDeep } from 'lodash';
// @ts-ignore
import { merge } from 'lodash';
import isObject from '../types/isObject';
import Request from '../interfaces/Request';
import RESTRequest from '../models/RESTRequest';
import RESTQueryConfiguration from '../models/RESTQueryConfiguration';
import GraphqlQueryConfiguration from '../models/GraphqlQueryConfiguration';
import DataStore from '../classes/DataStore';
import { module, actionModule } from '../types/module';
import { isRestNoGet } from '../types/Request';
import GraphqlRequest from './GraphqlRequest';

export interface CacheStoreArguments {
  state?: module;
  mutations?: actionModule;
  actions?: actionModule;
  getters?: actionModule;
  referenceKey?: string;
}

/**
 * Get the difference between an object and an array of keys
 * 
 * @export dataHasKeys
 * @function dataHasKeys
 * @param {any} data - the data to check
 * @param {Array<string>} searchKeys - the array of keys to check on the data
 * @return {Array<string>} - the keys missing
 */

export const dataHasKeys = (data: any, searchKeys: Array<string>): Array<string> => {
  const diff = difference(searchKeys, Object.keys(data));
  return diff;
}


/**
 * Check if an object has a certain array of keys
 * 
 * @param {any} data - the data to check
 * @param {Array<string>} searchKeys - the array of keys to check on the data
 * @return {Error | Array<string>} - An error on the object | an empty array
 */

export const checkDataWithReference = (data: any, searchKeys: Array<string>): Error | Array<string> => {
  const keyDifference = dataHasKeys(data, searchKeys);
  if (keyDifference.length > 0) return new Error(`${JSON.stringify(data)} does not contain searchKey ${keyDifference}`);
  else return keyDifference;
}
/**
  @hidden
*/
const _mutations = {
  'set:request-resolved': (state: module, request: Request, searchKeys: Array<string>): Request => {
    state.resolved[request[searchKeys[0]]] = request;
    return state.resolved[request[searchKeys[0]]];
  },
  'set:request-pending': (state: module, request: Request, searchKeys: Array<string>): Request => {
    state.pending[request[searchKeys[0]]] = request;
    return state.pending[request[searchKeys[0]]];
  },
  'set:request-failed': (state: module, request: Request, searchKeys: Array<string>): Request => {
    state.failed[request[searchKeys[0]]] = request;
    return state.failed[request[searchKeys[0]]];
  },
  'delete:request-resolved': (state: module, request: Request, searchKeys: Array<string>): void => {
    delete state.resolved[request[searchKeys[0]]];
  },
  'delete:request-pending': (state: module, request: Request, searchKeys: Array<string>): void => {
    delete state.pending[request[searchKeys[0]]];
  },
  'delete:request-failed': (state: module, request: Request, searchKeys: Array<string>): void => {
    delete state.failed[request[searchKeys[0]]];
  },
};
/**
  @hidden
*/
const _actions = {};
/**
  @hidden
*/
const _getters = {};

/**
 * Define the CacheStore class used to stock request and update the client store
 * 
 * @export CacheStore
 * @class CacheStore
 * @extends {DataStore}
 */

export class CacheStore extends DataStore {

  state: module;
  mutations: actionModule;
  actions: actionModule;
  getters: actionModule;
  referenceKey: string;

  constructor({
    state = {
      pending: {},
      resolved: {},
      failed: {},
    },
    mutations = _mutations,
    actions = _actions,
    getters = _getters,
    referenceKey = 'id'
  }: CacheStoreArguments) {
    super({state, mutations, actions, getters});
    this.referenceKey = referenceKey;
  }

  addBackup(request: Request): void {
    if (!request.cacheConfig.backup) {
      const state = request.cacheConfig.store.state[request.cacheConfig.storeModuleName];
      const searchQuery = { [this.referenceKey]: (request.queryConfig instanceof GraphqlQueryConfiguration ?
        request.queryConfig.variables[this.referenceKey]
        : request.queryConfig.body[this.referenceKey]) };
      const found = state instanceof Array ? findIndex(state, searchQuery) : (request.queryConfig instanceof GraphqlQueryConfiguration ?
        request.queryConfig.variables[this.referenceKey]
        : request.queryConfig.body[this.referenceKey]);
      if ((typeof found === 'number' && found >= 0) || (typeof found === 'string' && state[found])) request.cacheConfig.backup = cloneDeep(state[found]);
      else {
        request.cacheConfig.backup = request.cacheConfig.optimisticResponse || request.queryConfig.body;
        request.cacheConfig.new = true;
      }
    }
  }

  async updateClientCache(request: Request): Promise<any> {
    this.addBackup(request);
    if (request.response) {
      let json = request.response.statusText;
      try {
        json = await request.response.json();
      } catch(e) {}

      if (request.response.status >= 300 || request.status === 'FAILED') {
        if ((request.cacheConfig.optimisticResponse || isRestNoGet(request) || request instanceof GraphqlRequest)) {
          if (!request.cacheConfig.new && !request.cacheConfig.applied) {
            request.cacheConfig.store.commit(`set:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, request.cacheConfig.backup);
          } else {
            request.cacheConfig.store.commit(`delete:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, request.cacheConfig.backup);
          }
        }
        if (request.cacheConfig.errorCallback) request.cacheConfig.errorCallback(json);
      } 
      else if (request.response.status >= 200 || request.status === 'RESOLVED') {
        if (request.queryConfig.type === 'SIMPLE') {
          request.cacheConfig.store.commit(`set:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, json);
        }
        if (request.cacheConfig.successCallback) request.cacheConfig.successCallback(json);
      }
      request.cacheConfig.applied = true;
    } else {
      if (request.cacheConfig.optimisticResponse && !request.status) {
        request.cacheConfig.store.commit(`set:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, request.cacheConfig.optimisticResponse);
      } else if (isRestNoGet(request) && !request.status) {
        request.cacheConfig.store.commit(`set:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, JSON.parse(JSON.stringify(request.queryConfig.body)));
      } else if (request instanceof GraphqlRequest && !request.status) {
        const copy = cloneDeep(request.cacheConfig.backup);
        const save = request.cacheConfig.backup instanceof Array ? request.cacheConfig.backup : merge(copy, request.queryConfig.variables);
        request.cacheConfig.store.commit(`set:${request.cacheConfig.storeModuleName}`, request.cacheConfig.uniqueKeys, save);
      }
    }
  }

  commit(mutationName: string, searchKeys: Array<string>, data: any): any {
    return this.trigger('mutation', mutationName, [data, this.getSearchKeys(searchKeys)]);
  }

  dispatch(actionName: string, searchKeys: Array<string>, ...args: Array<any>): Promise<any> {
    return this.trigger('action', actionName, [this.commit, this.getSearchKeys(searchKeys), ...args]);
  }

  getSearchKeys(searchKeys: Array<string> = []) {
    searchKeys.unshift(this.referenceKey);
    return searchKeys;
  }
}