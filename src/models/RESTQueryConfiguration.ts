import { pollingStates, pollingState } from '../types/polling';
import { RESTMethod } from '../types/RESTMethod';

import { QueryConfiguration } from '../classes/QueryConfiguration';
import { isPollingState } from '../classes/QueryConfiguration';
import Body from './Body';

/**
 * Define the RESTQueryConfiguration
 * Contains all the data needed for a RESTRequest to work
 * 
 * @export default
 * @class RESTQueryConfiguration
 * @extends {QueryConfiguration}
 */

export default class RESTQueryConfiguration extends QueryConfiguration {

  type:string;
  url:string;
  method: RESTMethod;
  body: Body;
  pollingStates:pollingStates;

  constructor(
    method: RESTMethod,
    type:string,
    url:string,
    body: Body = new Body(),
    pollingStates?: pollingStates
  ) {
    if (pollingStates) super(type, url, pollingStates)
    else super(type, url)
    this.method = method || 'GET';
    this.body = body;
  }
}