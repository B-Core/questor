import { v1 } from 'uuid';
import gql from 'graphql-tag';

import { pollingStates, pollingState } from '../types/polling';
import { GraphqlMethod } from '../types/GraphqlMethod';

import { QueryConfiguration } from '../classes/QueryConfiguration';
import { isPollingState } from '../classes/QueryConfiguration';

/**
 * Define the GraphqlQueryConfiguration
 * Contains all the data needed for a GraphqlRequest to work
 * 
 * @export default
 * @class GraphqlQueryConfiguration
 * @extends {QueryConfiguration}
 */

export default class GraphqlQueryConfiguration extends QueryConfiguration {

  method: GraphqlMethod;
  type:string;
  url:string;
  query: string;
  variables: any;
  pollingStates:pollingStates;

  constructor(
    method: GraphqlMethod,
    type:string,
    url:string,
    query?: string,
    variables?: any,
    pollingStates?: pollingStates
  ) {
    if (pollingStates) super(type, url, pollingStates)
    else super(type, url)
    this.method = method;
    this.query = query;
    this.variables = variables;
  }

  get body() {
    return {
      operationName: this.definition.definitions[0].name.value,
      query: this.query,
      variables: this.variables,
    }
  }

  get definition() {
    return gql`${this.query}`;
  }
}