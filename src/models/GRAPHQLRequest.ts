import 'isomorphic-fetch';

import { v1 } from 'uuid';

import { headerArg } from '../types/header';
import { RESTMethod } from '../types/RESTMethod';

import Request from '../interfaces/Request';

import Protocol from '../classes/Protocol';
import QHeaders from '../models/QHeaders';
import CacheConfiguration from '../classes/CacheConfiguration';
import GraphqlQueryConfiguration from './GraphqlQueryConfiguration';
import Body from './Body';

/**
 * Defined the GraphqlRequest class
 * It represents a Request object formatted for a Graphql query or mutation
 * 
 * @export default
 * @class GraphqlRequest
 * @implements {Request}
 */

export default class GraphqlRequest implements Request {

  id: string;
  protocol: Protocol;
  queryConfig: GraphqlQueryConfiguration;
  cacheConfig: CacheConfiguration;
  headers: Headers;
  request: Promise<any>;
  response: Response;
  [key: string]: any;

  constructor(
    protocol: Protocol,
    queryConfig: GraphqlQueryConfiguration,
    cacheConfig: CacheConfiguration,
    headers: headerArg,
  ) {
    this.id = v1();
    this.protocol = protocol;
    this.queryConfig = queryConfig;
    this.cacheConfig = cacheConfig;
    this.headers = new QHeaders(headers);
    this.headers.append('Accept', 'application/json');
    this.headers.append('Content-Type', 'application/json');
  }

  send(url: string = this.queryConfig.url): Promise<any> {
    this.request = fetch(url, {
      method: 'POST',
      headers: this.headers,
      body: JSON.stringify(new Body(this.queryConfig.body)),
    });
    return this.request;
  }
}