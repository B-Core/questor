// @ts-ignore
import { findIndex } from 'lodash';
// @ts-ignore
import { find } from 'lodash';

import { header, isHeader, convertToHeaderKey } from '../types/header';
import headerguard from '../types/headerguard';
import { module } from '../types/module';
import isObject from '../types/isObject';
import Iterator from '../classes/Iterator';


/**
 * Define the QHeader class (because Header was already taken)
 * It represents the Header class that will be used and sent with every Request
 * 
 * @export default
 * @class QHeaders
 * @implements {Headers}
 * @implements {IterableIterator<[string, string]>}
 */

export default class QHeaders implements Headers, IterableIterator<[string, string]> {
  
  protected _headerlist: Array<[string, string]> = [];
  protected _currentIndex: number;
  guard: headerguard;

  constructor(headers: Array<[string, string]> | module = {}, guard: headerguard = 'none') {
    if (isObject(headers)) {
      const keys = Object.keys(headers);
      keys.forEach(key => this.append(key, headers[key]));
    } else if (headers instanceof Array && isHeader(headers[0])) {
      this._headerlist = headers;
    } else {
      throw new TypeError('invalid constructor arguments for QHeader, required Array<[string, string]> or [key: string]: any')
    }
    this._currentIndex = 0;
    this.guard = guard;
  }
  
  [Symbol.iterator](): IterableIterator<[string, string]> {
    return this;
  }

  public next(): IteratorResult<[string, string]> {
    const current = this._headerlist[this._currentIndex];
    let iterator = {
      done: false,
      value: current,
    };
    this._currentIndex += this._currentIndex <= this._headerlist.length ? 1 : 0;
    if (this._currentIndex > this._headerlist.length) {
      iterator.done = true;
      iterator.value = null;
    }
    return iterator;
  }

  /**
   * Returns an iterator allowing to go through all key/value pairs contained in this object.
   */
  entries(): IterableIterator<[string, string]> {
    return this;
  }
  /**
   * Returns an iterator allowing to go through all keys f the key/value pairs contained in this object.
   */
  keys(): IterableIterator<string> {
    return new Iterator(this._headerlist.map(header => header[0]));
  }
  /**
   * Returns an iterator allowing to go through all values of the key/value pairs contained in this object.
   */
  values(): IterableIterator<string> {
    return new Iterator(this._headerlist.map(header => header[1]));
  }

  append(name: string, value: string): void {
    const n = convertToHeaderKey(name);
    const index = findIndex(this._headerlist, (o: Array<string>) => new RegExp(n, 'ig').test(o[0]));
    if (index >= 0) {
      this.set(n, value);
    } else {
      const header = [n, value];
      if (isHeader(header)) this._headerlist.push([n, value]);
      else throw new TypeError(`${name} is not a valid header`);
    }
  }
  delete(name: string): void {
    const n = convertToHeaderKey(name);
    const index = findIndex(this._headerlist, (o: Array<string>) => new RegExp(n, 'ig').test(o[0]));

    if (index >= 0) this._headerlist.splice(index, 1);
    else throw new ReferenceError(`${name} is not a header`);
  }
  forEach(callback: ForEachCallback): void {
    this._headerlist.forEach((value, index, array) => {
      return callback(value, 'usable');
    });
  }
  get(name: string): string | null {
    const n = convertToHeaderKey(name);
    const result = find(this._headerlist, (o: Array<string>) => new RegExp(n, 'ig').test(o[0]));
    return result ? result[1] : null;
  }
  has(name: string): boolean {
    return !!(find(this._headerlist, (o: Array<string>) => new RegExp(name, 'ig').test(o[0])));
  }
  set(name: string, value: string): void {
    const n = convertToHeaderKey(name);
    const index = findIndex(this._headerlist, (o: Array<string>) => new RegExp(n, 'ig').test(o[0]));
    if (isHeader([n, value])) {
      if (index >= 0) this._headerlist[index] = [n, value];
      else this._headerlist.push([n, value]);
    } else throw new TypeError(`${name} is not a valid header`);
  }

  get length() {
    return this._headerlist.length;
  }
  get headers() {
    const headers: module = {};
    this._headerlist.forEach((header) => {
      headers[header[0]] = header[1];
    });
    return headers;
  }
}