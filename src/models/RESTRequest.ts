import 'isomorphic-fetch';

import { v1 } from 'uuid';

import { headerArg } from '../types/header';
import { RESTMethod } from '../types/RESTMethod';

import Request from '../interfaces/Request';

import Protocol from '../classes/Protocol';
import QHeaders from '../models/QHeaders';
import CacheConfiguration from '../classes/CacheConfiguration';
import RESTQueryConfiguration from './RESTQueryConfiguration';
import Body from './Body';

/**
 * Defined the RESTRequest class
 * It represents a Request object formatted for a REST request
 * 
 * @export default
 * @class RESTRequest
 * @implements {Request}
 */

export default class RESTRequest implements Request {

  id: string;
  protocol: Protocol;
  queryConfig: RESTQueryConfiguration;
  cacheConfig: CacheConfiguration;
  headers: Headers;
  request: Promise<any>;
  response: Response;
  [key: string]: any;

  constructor(
    protocol: Protocol,
    queryConfig: RESTQueryConfiguration,
    cacheConfig: CacheConfiguration,
    headers: headerArg,
  ) {
    this.id = v1();
    this.protocol = protocol;
    this.queryConfig = queryConfig;
    this.cacheConfig = cacheConfig;
    this.headers = new QHeaders(headers);
  }

  send(url: string = this.queryConfig.url): Promise<any> {
    const fetchConf: any = {
      method: this.queryConfig.method,
      headers: this.headers,
    };
    if (this.queryConfig.method !== 'GET') fetchConf.body = JSON.stringify(this.queryConfig.body);
    this.request = fetch(url, fetchConf);
    return this.request;
  }
}