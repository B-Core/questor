/**
 * Body implementation to use with every request following [fetch specification](https://fetch.spec.whatwg.org/)
 * 
 * @export default
 * @class Body
 * @implements {Body}
 */
export default class Body implements Body {
  readonly bodyUsed: boolean;
  [key:string]: any;

  constructor(json?: any) {
    for (const key in json) {
      this[key] = json[key];
    }
  }
}