/**
 * Define what a Request has to do be treated and useful for Questor client
 * 
 * @export Request
 * @interface Request
 */


import Protocol from '../classes/Protocol';
import CacheConfiguration from '../classes/CacheConfiguration';
import RESTQueryConfiguration from '../models/RESTQueryConfiguration';
import GraphqlQueryConfiguration from '../models/GraphqlQueryConfiguration';
import Body from '../models/Body';

export default interface Request {

  id: string;
  protocol: Protocol;
  queryConfig: RESTQueryConfiguration | GraphqlQueryConfiguration;
  cacheConfig: CacheConfiguration;
  headers: Headers;
  response: Response;
  request: Promise<any>;
  [key: string]: any;

  send(url?: string): Promise<any>

}