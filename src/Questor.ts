// @ts-ignore
import { defaults } from 'lodash';

import { headerArg } from './types/header';
import { QueryConfig } from './types/QueryConfig';
import { GraphqlMethod } from './types/GraphqlMethod';
import { RESTMethod } from './types/RESTMethod';
import { Request } from './types/Request';

import Protocol from './classes/Protocol';
import CacheConfiguration from './classes/CacheConfiguration';
import Body from './models/Body';
import QHeaders from './models/QHeaders';
import RESTQueryConfiguration from './models/RESTQueryConfiguration';
import RESTRequest from './models/RESTRequest';
import GraphqlQueryConfiguration from './models/GraphqlQueryConfiguration';
import GraphqlRequest from './models/GraphqlRequest';
import DataStore from './classes/DataStore';
import QueryManager from './managers/QueryManager';
import { CacheStore } from './models/CacheStore';

export interface QuestorConstructorArguments {
  protocol?: Protocol
  cacheConfig?: CacheConfiguration
  queryConfig?: QueryConfig
  headers?: headerArg
  store?: DataStore
};
/**
  @hidden
*/
const defaultStore = new DataStore({});

/**
 * Define the Questor class
 * A Questor instance can createRequest and all of the necessary objects
 * 
 * @export default
 * @class Questor
 */

export default class Questor {

  protocol: Protocol;
  cacheConfig: CacheConfiguration;
  queryConfig: QueryConfig;
  queryManager: QueryManager;
  headers: Headers;
  store: DataStore;
  
  constructor({
    protocol = new Protocol('graphql'),
    cacheConfig = new CacheConfiguration('cache', defaultStore),
    queryConfig = new GraphqlQueryConfiguration('query', 'simple', 'https://test.com'),
    headers = {},
    store = defaultStore,
  }: QuestorConstructorArguments) {
    this.protocol = protocol;
    this.cacheConfig = cacheConfig;
    this.queryConfig = queryConfig;
    this.queryManager = new QueryManager();
    this.headers = new QHeaders(headers);
  }

  createRequest(
    protocol: Protocol = this.protocol,
    queryConfig: QueryConfig = this.queryConfig,
    cacheConfig: CacheConfiguration = this.cacheConfig,
    headers: headerArg = {},
  ): Request {
    switch(protocol.type) {
      case 'GRAPHQL': {
        if (queryConfig instanceof GraphqlQueryConfiguration) {
          const graphQueryConf = queryConfig as GraphqlQueryConfiguration;
          return new GraphqlRequest(protocol, graphQueryConf, cacheConfig, headers);
        } else {
          throw new TypeError(`queryConfig is not a GraphqlQueryConfiguration but ${queryConfig.constructor.name}`);
        }
      }
      case 'REST': {
        if (queryConfig instanceof RESTQueryConfiguration) {
          const restQueryConf = queryConfig as RESTQueryConfiguration;
          return new RESTRequest(protocol, restQueryConf, cacheConfig, headers);
        } else {
          throw new TypeError(`queryConfig is not a RESTQueryConfiguration but ${queryConfig.constructor.name}`);
        }
      }
      default: {
        throw new TypeError(`${protocol.type} is not a known protocol`);
      }
    }
  }

  static createCacheConfiguration(params: any): CacheConfiguration {
    return new CacheConfiguration(params.type, params.store, params.uniqueKeys, params.storeModuleName, params.optimisticResponse);
  }

  static createQueryConfiguration(type: string, params: any): QueryConfig {
    switch(type) {
      case 'graphql': {
        return new GraphqlQueryConfiguration(params.method, params.type, params.url, params.query, params.variables, params.pollingStates);
      }
      case 'rest': {
        return new RESTQueryConfiguration(params.method, params.type, params.url, params.body, params.pollingStates);
      }
      default: {
        throw new TypeError(`invalid type for configuration: ${type}`)
      }
    }
  }

  add(...requests: Array<Request>) {
    return this.queryManager.add(...requests);
  }
  async send(request: Request): Promise<any> {
    return await this.queryManager.execute(request);
  }
  async sendAll(): Promise<any> {
    return await this.queryManager.executeAll();
  }
}