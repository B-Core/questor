/**
 * Define the accepted cacheTypes for a CacheConfiguration
 * 
 * @export cacheTypes
 * @enum {string}
 */

enum cacheTypes {
  'NO-CACHE' = 'NO-CACHE',
  'CACHE' = 'CACHE',
}

export default cacheTypes;