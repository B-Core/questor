/**
 * Define the accepted types of queries for the QueryConfigurations
 * 
 * @export queryTypes
 * @enum {string}
 */


enum queryTypes {
  'SIMPLE' = 'SIMPLE',
  'POLLING' = 'POLLING',
}

export default queryTypes;