/**
 * Define the accepted protocols for the Protocol class
 * 
 * @export default
 * @enum {string}
 */

enum protocols {
  'REST' = 'REST',
  'GRAPHQL' = 'GRAPHQL',
}

export default protocols;