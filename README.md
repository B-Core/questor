# Questor

> A Javascript client to communicate with services using various protocols

```
Questor
  createRequest
  sendRequest

  Requests
    GraphqlRequest
    RESTRequest

  CacheManager
    createCacheObject
    updateCacheObject

  CacheStore
    getClientStoreBackup
    restoreClientStoreBackup
    updateClientStore
    triggerCallbacks
    setRequestStore
    getRequestStore

  QueryManager
    send
    poll
```